<?php

namespace App\Http\Controllers;

use App\Helpers\APIHelper;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\Group;
use App\Models\Hotline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function adminLogin()
    {
        return view('admin.login');
    }
    function adminCheck(Request $request){
        //Validate requests
        $request->validate([
            'email'=>'required',
            'password'=>'required|min:5|max:12'
        ]);
        $admin = Admin::where('email','=', $request->email)->first();
        if(!$admin){
            return back()->with('fail','Tài khoản chưa được đăng ký!');
        }else{
            //check password
            if(Hash::check($request->password, $admin->password)){
                $request->session()->put('LoggedAdmin', $admin);
                return redirect('adminHome');
            }else{
                return back()->with('fail','Mật khẩu không chính xác');
            }
        }
    }

    public function adminHome()
    {
        $data['list'] = DB::table('users')->orderBy('id','DESC')->paginate(5);
        return view('admin.admin', $data);
    }
    public function adminHotline(){
        $dataHotline['list'] = DB::table('hotline')->orderBy('id','DESC')->paginate(5);
        return view('admin.adminHotline', $dataHotline);
    }

    public function edit($id){
        $row = DB::table('hotline')->where('id',$id)->first();
        $editHotline = [
            'Info'=>$row
        ];
        return view('admin.editHotline',$editHotline);
    }
    public function updateHotline(Request $request){
        $data = $request->all();
        $updateHotline = Hotline::find($data['id']);
        $updateHotline -> hotline_number = $data['hotline_number'];
        $updateHotline -> telecom_operator = $data['telecom_operator'];
        if ($request->hasFile('contract_image')){
            $file = $request->file('contract_image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/contract_image',$filename);
            $updateHotline->contract_image = $filename;
        }
        if($data['status'] == true){
            $updateHotline -> status = $data['status'] = true;
        }else{
            $updateHotline -> status = $data['status'] = false;
        }

        $updateHotline -> save();
        return redirect('adminHotline')->with('success','Cập nhật thành công!');
    }


    function adminLogout()
    {
        if (session()->has('LoggedAdmin')) {
            session()->pull('LoggedAdmin');
            return redirect('adminLogin');
        }
    }
    public function addUser(Request $request){
        $request->validate([
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users|','regex:/(9|3|7|8|5)[0-9]{8}/',
            'password' => 'required'
        ],
            [
                'name.required' => 'Bạn vui lòng nhập tên doanh nghiệp',
                'name.unique' => 'Tên doanh nghiệp đã tồn tại, vui lòng chọn tên khác',
                'email.required' => 'Bạn vui lòng nhập email',
                'email.unique' => 'Email đã được đăng ký, vui lòng chọn email khác',
                'phone.required' => 'Bạn vui lòng nhập số hotline',
                'phone.unique' => 'Số hotline đã được đăng ký, bạn vui lòng chọn số khác',
                'password.required' => 'Bạn vui lòng nhập mật khẩu',
                'password.same' => 'Mật khẩu và mật khẩu xác nhận phải giống nhau',
                'password.min' => 'Mật khẩu tối thiểu phải có 6 ký tự',
                'password.max' => 'Mật khẩu tối đa là 20 ký tự',
                'password-confirm.required' => 'Bạn vui lòng xác nhận mật khẩu',
            ]);
        $query = DB::table('users')->insert([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'taxcode' => $request->input('taxcode'),
            'address' => $request->input('address'),
            'job' => $request->input('job'),
            'password' => Hash::make($request->input('password')),
        ]);
        if ($query) {
            return back()->with('success', 'Thêm user thành công!');
        } else {
            return back()->with('fail', 'Thêm user không thành công!');
        }

    }
    function updateUser(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users|','regex:/(9|3|7|8|5)[0-9]{8}/',
            'password' => 'required'
        ],
            [
                'name.required' => 'Bạn vui lòng nhập tên doanh nghiệp',
                'name.unique' => 'Tên doanh nghiệp đã tồn tại, vui lòng chọn tên khác',
                'email.required' => 'Bạn vui lòng nhập email',
                'email.unique' => 'Email đã được đăng ký, vui lòng chọn email khác',
                'phone.required' => 'Bạn vui lòng nhập số hotline',
                'phone.unique' => 'Số hotline đã được đăng ký, bạn vui lòng chọn số khác',
                'password.required' => 'Bạn vui lòng nhập mật khẩu',
                'password.same' => 'Mật khẩu và mật khẩu xác nhận phải giống nhau',
                'password.min' => 'Mật khẩu tối thiểu phải có 6 ký tự',
                'password.max' => 'Mật khẩu tối đa là 20 ký tự',
                'password-confirm.required' => 'Bạn vui lòng xác nhận mật khẩu',
            ]);

        $updateUser = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'taxcode' => $request->taxcode,
            'address' => $request->address,
            'job' => $request->job,
            'password' => Hash::make($request->password),

        ];
        DB::table('users')->where('id', $request->id)->update($updateUser);
        return redirect()->back()->with('success', 'Cập nhật thành công!');
    }

    public function deleteUser($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }

    public function adminAgent(){
        $data['list'] = DB::table('agent')->orderBy('id','DESC')->paginate(5);
        $data['group'] = Group::all();
        return view('admin.adminAgent',$data);
    }

    public function updateAdminAgent(Request $request){
        $request->validate([
            'member_email' => 'required|string|email|unique:agent'
        ],[
            'member_email.unique' => 'Email đã được đăng ký, vui lòng chọn email khác'
        ]);
        $update = [
            'member_name' => $request->member_name,
            'member_address' => $request->member_address,
            'member_phone' => $request->member_phone,
            'member_email' => $request->member_email,
            'member_password' => Hash::make($request->member_password),

        ];
        DB::table('agent')->where('id', $request->id)->update($update);
        return redirect()->back()->with('success', 'Cập nhật thành công!');
    }
    public function deleteAdminAgent($id)
    {
        DB::table('agent')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }

}

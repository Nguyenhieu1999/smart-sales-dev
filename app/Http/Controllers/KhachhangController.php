<?php

namespace App\Http\Controllers;

use App\Imports\CustomerImport;
use App\Models\Agent;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class KhachhangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer()
    {
        $data['customer'] = DB::table('customers')->paginate(15);
        return view('khachhang', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadCustomer(Request $request)
    {
        $request->validate([
            'phone' => 'required','regex:/(9|3|7|8|5)[0-9]{8}/',

        ]);
        $tag = $request->tag;
        $usId = Session::get('LoggedUser')->id;

        $rows = Excel::toArray(new CustomerImport, $request->file);
        $data = $rows[0];

        foreach ($data as $key => $value) {
            try {
                $customer = new Customer();
                $customer->name = $value['name'];
                $customer->address = $value['address'];
                $customer->phone = $value['phone'];
                $customer->tag = $tag;
                $customer->us_id = $usId;

                $customer->save();
            } catch (\Exception $exception) {
                return dd($exception);

            }
        }
        return redirect()->back()->with('success', 'Upload khách hàng thành công');
    }

    public function addCustomer(Request $request)
    {
        $usId = Session::get('LoggedUser')->id;
        $request->validate([
            'name' => 'required',
            'phone' => 'required','regex:/(9|3|7|8|5)[0-9]{8}/',
            'address' => 'required',
            'tag' => 'required'
        ]);

        $data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'tag' => $request->tag,
            'us_id' => $usId
        ];

        DB::table('customers')->insert($data);
        return back()->with('success', 'Thêm khách hàng thành công!');
    }

    public function updateCustomer(Request $request)
    {

        $update = [
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'tag' => $request->tags,

        ];
        DB::table('customers')->where('id', $request->id)->update($update);
        return redirect()->back()->with('success', 'Cập nhật thành công!');
    }

    public function deleteCustomer($id)
    {
        DB::table('customers')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Models\SurveyAnswer;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class KhaosatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function survey()
    {
        $survey_list['list'] = DB::table('survey')->paginate(15);
        return view('khaosat',$survey_list);
    }
    public function addSurvey(Request $request){

        $data = $request->all();
        $usId = Session::get('LoggedUser') ->id;
        //save survey
        $survey = new Survey;
        $survey -> survey_name = $data['survey_name'];
        $survey -> survey_type = $data['survey_type'];
        $survey -> survey_question = $data['survey_question'];
        $survey -> us_id = $usId;
        $survey -> save();

        //save survey answer
        if ($data['survey_type'] == 1){
            $surveyAnswer = new SurveyAnswer;
            $surveyAnswer -> survey_id = $survey ->id;
            $surveyAnswer -> answer = $data['survey_answer'];
            $surveyAnswer->save();
        }

        if ($data['survey_type'] == 2){
            foreach ($request->survey_multi_answer as $index => $answer) {
                $surveyAnswer = new SurveyAnswer;
                $surveyAnswer -> survey_id = $survey ->id;
                $surveyAnswer -> answer = $answer;
                $surveyAnswer->save();
            }

        }

        if ($data['survey_type'] == 3){
            $surveyAnswer = new SurveyAnswer;
            $surveyAnswer -> survey_id = $survey ->id;
            $surveyAnswer -> survey_score_min = $data['survey_score_min'];
            $surveyAnswer -> survey_score_max = $data['survey_score_max'];

            $surveyAnswer->save();
        }

        return back()->with('success','Thêm khảo sát thành công!');
    }
    public function updateSurvey(Request $request){

        $data = $request->all();
        $usId = Session::get('LoggedUser') ->id;
        //save survey
        $survey = Survey::find($data['edit_id']);
        $survey -> survey_name = $data['edit_survey_name'];
        $survey -> survey_type = $data['edit_survey_type'];
        $survey -> survey_question = $data['edit_survey_question'];
        $survey -> us_id = $usId;
        $survey -> save();

        //xoa het answer xong insert lai
        $survey ->listOfAnswer() ->delete();

        //save survey answer
        if ($data['edit_survey_type'] == 1){
            $surveyAnswer = new SurveyAnswer;
            $surveyAnswer -> survey_id = $survey ->id;
            $surveyAnswer -> answer = $data['edit_survey_answer'];
            $surveyAnswer->save();
        }

        if ($data['edit_survey_type'] == 2){
            foreach ($request->edit_survey_multi_answer as $index => $answer) {
                $surveyAnswer = new SurveyAnswer;
                $surveyAnswer -> survey_id = $survey ->id;
                $surveyAnswer -> answer = $answer;
                $surveyAnswer->save();
            }

        }

        if ($data['edit_survey_type'] == 3){
            $surveyAnswer = new SurveyAnswer;
            $surveyAnswer -> survey_id = $survey ->id;
            $surveyAnswer -> survey_score_min = $data['edit_survey_score_min'];
            $surveyAnswer -> survey_score_max = $data['edit_survey_score_max'];

            $surveyAnswer->save();
        }

        return redirect()->back()->with('success','Cập nhật thành công!');
    }

    public function updateSurveyInfo(Request $request){

        try {
            $survey = Survey::find($request -> updateSurveyInfoSurveyID);

            $reponseData['result'] = true;
            $reponseData['survey'] = $survey;
            $reponseData['surveyAnswer'] = $survey -> listOfAnswer() -> get();
        }catch (Exception $e){
            $reponseData['result'] = false;
            $reponseData['survey'] = null;
            $reponseData['surveyAnswer'] = null;
        }
        return response()->json($reponseData);

    }

    public function deleteSurvey($id)
    {
        //DB::table('survey')->where('id', $id)->delete();
        $survey = Survey::find($id);
        $survey ->listOfAnswer() ->delete();
        $survey -> delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }

}

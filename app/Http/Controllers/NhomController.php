<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class NhomController extends Controller
{

    public function group(Request $request)
    {
        $data['list'] = Group::all();
        return view('nhom', $data);
    }
    public function addGroup(Request $request)
    {
        $request->validate([
            'group_name' => 'required|unique:groups',
            'tag' => 'required'
        ]);
        $usId = Session::get('LoggedUser') ->id;
        $query = DB::table('groups')->insert([
            'group_name' => $request->input('group_name'),
            'tag' => $request->input('tag'),
            'us_id'=>$usId
        ]);

        if ($query) {
            return back()->with('success', 'Thêm nhóm thành công!');
        } else {
            return back()->with('fail', 'Thêm nhóm không thành công!');
        }
    }

    function update(Request $request)
    {
        $update = [
            'group_name' => $request->group_name,
            'tag' => $request->tag,

        ];
        DB::table('groups')->where('id', $request->id)->update($update);
        return redirect()->back()->with('success', 'Cập nhật thành công!');
    }

    public function delete($id)
    {
        DB::table('groups')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }

}

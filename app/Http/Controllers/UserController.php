<?php

namespace App\Http\Controllers;
use App\Helpers\APIHelper;
use App\Models\Agent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register()
    {
        return view('register');
    }

    public function login()
    {
        return view('login');
    }

    protected function create(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:11|unique:users','regex:/(9|3|7|8|5)[0-9]{8}/',
            'password' => 'required|min:6|max:20|required_with:password-confirm|same:password-confirm',
            'password-confirm' => 'required|min:6|max:20'
        ],
            [
                'name.required' => 'Bạn vui lòng nhập tên doanh nghiệp',
                'email.required' => 'Bạn vui lòng nhập email',
                'email.unique' => 'Email đã được đăng ký, vui lòng chọn email khác',
                'phone.required' => 'Bạn vui lòng nhập số hotline',
                'phone.unique' => 'Số hotline đã được đăng ký vui lòng chọn số khác',
                'password.required' => 'Bạn vui lòng nhập mật khẩu',
                'password.same' => 'Mật khẩu và mật khẩu xác nhận phải giống nhau',
                'password.min' => 'Mật khẩu tối thiểu phải có 6 ký tự',
                'password.max' => 'Mật khẩu tối đa là 20 ký tự',
                'password-confirm.required' => 'Bạn vui lòng xác nhận mật khẩu',
            ]);
        $data = $request->all();
        $user = new User;
        $user -> name = $data['name'];
        $user -> email = $data['email'];
        $user -> phone = $data['phone'];
        $user -> slug = Str::slug($data['name'],'-');
        $user -> api_key = $data['api_key'] = Str::uuid()->toString();
        $user -> role_id = 1;
        $user -> password = Hash::make($data['password']);
        $user -> save();
        return redirect('/')->with('success','Tạo tài khoản thành công!');
    }

    function check(Request $request)
    {
        //Validate requests
        $request->validate([
            'email' => 'required',
            'password' => 'required|min:5|max:12',
        ]);
        $user = User::where('email', '=', $request->email)->first();
        $agent = Agent::where('member_email','=',$request->email)->first();
        if (!$user || !$agent) {
            return back()->with('fail', 'Tài khoản chưa được đăng ký!');
        } else {
            //check password
            if (Hash::check($request->password, $user->password)) {
                $request->session()->put('LoggedUser', $user);
                return redirect('home');
            } elseif (Hash::check($request->password, $agent->member_password)){
                $request->session()->put('LoggedAgent', $agent);
                return redirect('home');
            } else {
                return back()->with('fail', 'Mật khẩu không chính xác');
            }
        }

    }

    public function home()
    {
        return view("home");
    }

    function logout()
    {
        if (session()->has('LoggedUser')) {
            session()->pull('LoggedUser');
            return redirect('/');
        }elseif (session()->has('LoggedAgent')){
            session()->pull('LoggedAgent');
            return redirect('/');
        }
    }

    public function profile($id)
    {
        $row = DB::table('users')->where('id', $id)->first();
        $data['Info'] = $row;
        return view('profile', $data);
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'job' => 'required',
            'phone' => 'required',
            'taxcode' => 'required',
            'address' => 'required'
        ],
            [
                'name.required' => 'Bạn vui lòng nhập tên doanh nghiệp',
                'email.required' => 'Bạn vui lòng nhập email',
                'email.unique' => 'Email đã được đăng ký, vui lòng chọn email khác',
                'phone.required' => 'Bạn vui lòng nhập số hotline',
                'job.required' => 'Bạn vui lòng nhập nghề nghiệp',
                'taxcode.required' => 'Bạn vui lòng nhập mã số thuế',
                'address.required' => 'Bạn vui lòng nhập mật khẩu',
                'password.max' => 'Mật khẩu tối đa là 20 ký tự',
                'password-confirm.required' => 'Bạn vui lòng xác nhận mật khẩu',
            ]);
        $updateProfile = DB::table('users')->where('id', $request->input('cid'))->update([
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name'), '-'),
            'email' => $request->input('email'),
            'job' => $request->input('job'),
            'phone' => $request->input('phone'),
            'taxcode' => $request->input('taxcode'),
            'address' => $request->input('address')
        ]);
        return redirect()->back()->with('success', 'Sửa hồ sơ thành công!');
    }


    public function apiCreateUser(Request $request){
        $rule = array(
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:11|unique:users','regex:/(9|3|7|8|5)[0-9]{8}/',
        );
        $validator = Validator::make($request->all(),$rule);
        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }else{
            $user = new User;
            $user -> name = $request->input('name');
            $user -> email = $request->input('email');
            $user -> phone = $request->input('phone');
            $user -> password = Hash::make($request->password = 123456789);
            $user -> slug = Str::slug($request->input('name'));
            $user -> api_key = Str::uuid()->toString();
            $user -> role_id = 1;
            $user -> save();
            $response = [
                'message' => 'success',
                'status' => 200,
                'data' => $user
            ];
            return response($response, 201);
        }
    }
    public function apiAuthUser(Request $request){
        $rule = array(
            'api_key' => 'required'
        );
        $validator = Validator::make($request->all(),$rule);
        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors(),
                'status' => 400
            ]);
        }
        $user = User::where('api_key',$request->api_key)->first();

        $token = $user->createToken('authToken')->plainTextToken;
        return response()->json([
            'status' => 200,
            'token' => $token
        ]);
    }
    public function apiGetUser(Request $request){
        $getUser = User::all();
        $response = APIHelper::createAPIResponse(false,200,'',$getUser);
        return response()->json($response,200);

    }


}

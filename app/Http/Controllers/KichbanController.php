<?php

namespace App\Http\Controllers;

use App\Models\CallScript;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class KichbanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function callscript()
    {
        $data['call_script'] = DB::table('call_script')->paginate(15);
        return view('kichban',$data);
    }
    public function editcallscript($id){
        $row = DB::table('call_script')->where('id',$id)->first();
        $editscript = [
            'Info'=>$row
        ];
        return view('suakichban',$editscript);
    }
    public function updateCallScript(Request $request){
        $request->validate([
            'call_script_name'=>'required',
            'script'=>'required'
        ]);
        $updating = DB::table('call_script')->where('id',$request->input('cid'))->update([
            'call_script_name'=>$request->input('call_script_name'),
            'script'=>$request->input('script')
        ]);
        return redirect('callscript')->with('success','Cập nhật thành công!');

    }
    public function deleteScript($id){
        $deleteScrip = DB::table('call_script')->where('id',$id)->delete();
        return redirect('callscript')->with('success','Xóa thành công!');

    }
    public function addScript(){
        return view('themkichban');
    }
    public function addCallScript(Request $request)
    {
        $request->validate([
            'call_script_name' => 'required|unique:call_script',
            'script' => 'required',
        ]);
        $usId = Session::get('LoggedUser') ->id;
        $data = [
            'call_script_name' => $request->call_script_name,
            'script' => $request->script,
            'us_id' => $usId
        ];

        DB::table('call_script')->insert($data);
        return redirect('callscript')->with('success', 'Thêm kịch bản thành công!');
    }

}

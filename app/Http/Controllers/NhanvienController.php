<?php

namespace App\Http\Controllers;

use App\Helpers\APIHelper;
use App\Imports\AgentImport;
use App\Models\Agent;
use App\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class NhanvienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAgent(Request $request, $id)
    {
        $data['listAgent'] = DB::table('agent')->where('group_id','=',$id)->paginate(15);
        $data['group'] = Group::find($id);
        return view('nhanvien',$data);
    }

    public function addAgent(Request $request)
    {
        $request->validate([
            'member_name' => 'required',
            'member_phone' => 'required',
            'member_address' => 'required',
            'member_email' => 'required|string|email|unique:agent',
            'member_password' => 'required|min:6|max:20',
        ]);


        $data = [
            'member_name' => $request->member_name,
            'member_phone' => $request->member_phone,
            'member_address' => $request->member_address,
            'member_email' => $request->member_email,
            'member_password' => Hash::make($request->member_password),
            'group_id' => $request->member_group_id
        ];

        DB::table('agent')->insert($data);
        return back()->with('success', 'Thêm nhân viên thành công!');
    }

    public function updateAgent(Request $request)
    {
        $update = [
            'member_name' => $request->member_name,
            'member_address' => $request->member_address,
            'member_phone' => $request->member_phone,
            'member_email' => $request->member_email,
            'member_password' => Hash::make($request->member_password),

        ];
        DB::table('agent')->where('id', $request->id)->update($update);
        return redirect()->back()->with('success', 'Cập nhật thành công!');
    }

    public function deleteAgent($id)
    {
        DB::table('agent')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }
    public function uploadAgent(Request $request)
    {
        $groupid =  $request->member_group_id;
        $rows = Excel::toArray(new AgentImport, $request->file);
        $data = $rows[0];
        foreach ($data as $key => $value) {
            try {

                $upload = new Agent();
                $upload ->member_name = $value['member_name'];
                $upload ->member_phone = $value['member_phone'];
                $upload ->member_address = $value['member_address'];
                $upload->member_email = $value['member_email'];
                $upload->member_password = Hash::make($value['member_password']);
                $upload->group_id = $groupid;

                $upload->save();
            }catch (\Exception $exception) {
                return dd($exception);

            }
        }
        return redirect()->back()->with('success','Upload nhân viên thành công!');
    }

    public function apiCreateAgent(Request $request){
        $rule = array(
            'member_name' => 'required|string|max:255',
            'member_email' => 'required|string|email|max:255|unique:agent',
            'member_phone' => 'required|string|max:11|unique:agent','regex:/(9|3|7|8|5)[0-9]{8}/',
        );
        $validator = Validator::make($request->all(),$rule);
        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }else{
            $agent = new Agent;
            $agent -> member_name = $request->input('member_name');
            $agent -> member_phone = $request->input('member_phone');
            $agent -> member_email = $request->input('member_email');
            $agent -> member_password = Hash::make($request->password = 123456789);
            $agent -> role_id = $request->role_id =2 ;
            $agent -> group_id = $request->group_id = 5;
            $agent -> save();
            $response = APIHelper::createAPIResponse(false,200,'Tạo doanh nghiệp thành công!',$agent);
            return response()->json($response,200);
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Helpers\APIHelper;
use App\Models\Hotline;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HotlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function hotline()
    {
        $hotlineList['list'] = DB::table('hotline')->paginate(15);
        return view('hotline',$hotlineList);
    }
    public function addHotline(Request $request){
        $request->validate([
            'hotline_number' => 'required|unique:hotline','regex:/(9|3|7|8|5)[0-9]{8}/'
        ],
        [
            'hotline_number.required' => 'Vui lòng nhập số hotline!',
            'hotline_number.unique' => 'Số hotline đã tồn tại, vui lòng nhập số khác!'
        ]);
        $data = $request->all();
        $usId = Session::get('LoggedUser') ->id;
        $hotline = new Hotline;
        $hotline -> hotline_number = $data['hotline_number'];
        $hotline -> telecom_operator = $data['telecom_operator'];
        $hotline -> us_id = $usId;
        if ($request->hasFile('contract_image')){
            $file = $request->file('contract_image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/contract_image',$filename);
            $hotline->contract_image = $filename;
        }else{
            return $request;
            $hotline->contract_image = '';
        }
        $hotline -> save();

        return back()->with('success','Thêm số hotline thành công!');
    }

    public function editHotline(Request $request){
        $data = $request->all();
        $usId = Session::get('LoggedUser') ->id;
        $edit = Hotline::find($data['id']);
        $edit -> hotline_number = $data['hotline_number'];
        $edit -> telecom_operator = $data['telecom_operator'];
        $edit -> us_id = $usId;
        if ($request->hasFile('contract_image')){
            $file = $request->file('contract_image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/contract_image',$filename);
            $edit->contract_image = $filename;
        }
        $edit -> save();
        return redirect()->back()->with('success','Cập nhật thành công!');
    }

    public function deleteHotline($id)
    {
        DB::table('hotline')->where('id', $id)->delete();
        return redirect()->back()->with('success', 'Xóa thành công! ');
    }

    public function apiHotlineUser(Request $request){
        $rule = array(
            'hotline_number' => 'required|unique:hotline|max:10','regex:/(9|3|7|8|5)[0-9]{8}/',
            'hotline_number.required' => 'Vui lòng nhập số hotline!',
            'hotline_number.unique' => 'Số hotline đã tồn tại, vui lòng nhập số khác!',
            'hotline_number.regex' => 'Số hotline sai định dạng, vui lòng nhập lại'
        );
        $validator = Validator::make($request->all(),$rule);
        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }else{

            $hotline = new Hotline;
            $hotline -> hotline_number = $request->input('hotline_number');
            $hotline -> telecom_operator = $request->input('telecom_operator');
            $hotline -> status = 0;
            $hotline -> save();
            $response = APIHelper::createAPIResponse(false,200,'Gán hotline cho doanh nghiệp thành công!',$hotline);
            return response()->json($response,200);
        }
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Survey extends Model
{
    use HasFactory, Notifiable;
    protected $table='survey';
    public $timestamps=false;
    protected $primaryKey="id";
    protected $fillable = [
        'us_id',
        'survey_name',
        'survey_type',
        'survey_question',
        'created_at	',
    ];

    // Category model
    public function listOfAnswer()
    {
        return $this->hasMany(SurveyAnswer::class, 'survey_id', 'id');
    }

}

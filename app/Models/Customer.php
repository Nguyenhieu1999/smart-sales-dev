<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use HasFactory, Notifiable;
    protected $table='customers';
    public $timestamps=false;
    protected $primaryKey="id";
    protected $fillable = [
        'name',
        'address',
        'phone',
        'tag',
        'created_at',
        'updated_at',
        'us_id'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
class Admin extends Model
{
    use Uuid;
    use HasFactory, Notifiable;
    protected $table='admin';
    public $timestamps=false;
    protected $primaryKey="admin_id";
    protected $fillable = [
        'email',
        'password',
        'created_at',
    ];
}

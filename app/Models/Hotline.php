<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Hotline extends Model
{
    use HasFactory, Notifiable;
    protected $table='hotline';
    protected $primaryKey="id";
    protected $fillable = [
        'us_id',
        'hotline_number',
        'telecom_operator',
        'contract_image',
        'status',
        'created_at',
        'updated_at'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyAnswer extends Model
{
    use HasFactory;

    protected $table='survey_answer';
    public $timestamps=false;
    protected $primaryKey="id";
    protected $fillable = [
        'survey_id',
        'answer',
        'survey_score_min',
        'survey_score_max',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Agent extends Model
{
    use HasFactory, Notifiable;
    protected $table='agent';
    public $timestamps=false;
    protected $primaryKey="id";
    protected $fillable = [
        'member_name',
        'member_phone',
        'member_address',
        'member_email',
        'member_password',
        'role_id',
        'group_id',
        'created_at',
        'updated_at',
    ];
}

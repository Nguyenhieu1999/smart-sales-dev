<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Group extends Model
{
    use HasFactory, Notifiable;
    protected $table='groups';
    protected $primaryKey="id";
    protected $fillable = [
        'id',
        'us_id',
        'group_name',
        'member_number',
        'tag',
        'created_at',
        'updated_at',
    ];

    // Category model
    public function listOfAgent()
    {
        return $this->hasMany(Agent::class, 'group_id', 'id');
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CallScript extends Model
{
    use HasFactory, Notifiable;
    protected $table='call_script';
    protected $primaryKey="id";
    protected $fillable = [
        'us_id',
        'name',
        'script',
        'created_at',
        'updated_at',

    ];
}

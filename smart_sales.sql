-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 21, 2021 lúc 02:01 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `smart_sales`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `agent`
--

CREATE TABLE `agent` (
  `id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `member_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_phone` int(11) NOT NULL,
  `member_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `agent`
--

INSERT INTO `agent` (`id`, `group_id`, `member_name`, `member_phone`, `member_address`, `member_email`, `member_password`, `created_at`, `updated_at`) VALUES
(31, 1, 'Hiếu Nguyễn1', 982699437, 'Hà Nội', 'oisonan1999@gmail.com', '$2y$10$kR4BPV/TApCtgVoT5okufOgyfxEpEr06PGBBw9Ohxe9yYKtE/r.K2', '2021-05-20 10:10:58', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `call_script`
--

CREATE TABLE `call_script` (
  `id` bigint(20) NOT NULL,
  `call_script_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `script` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `call_script`
--

INSERT INTO `call_script` (`id`, `call_script_name`, `script`, `created_at`, `updated_at`) VALUES
(1, 'Kịch bản 2', '<b>- Telesale chứng khoán</b>: Xin lỗi đây có phải là số máy của anh A không ạ? <br>\r\n\r\n                  <b>- A:</b> Tôi là A nghe đây<br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Em chào anh. Em là B, gọi điện cho anh từ công ty C. Hiện tại, bên em có một số cổ phiếu ABC em nghĩ rằng anh sẽ không bỏ qua. Anh có thể dành cho em 3 phút chứ ạ?<br>\r\n\r\n                  <b>- A:</b> Anh không quan tâm em ơi<br>\r\n\r\n                  <b>- Telesale chứng khoán: </b>Đây là một cơ hội đầu tư tốt mà công ty em chỉ dành cho một số lượng khách hàng giới hạn. Anh có thể cho em thời gian để nói qua về cơ hội này với anh được không ạ?<br>\r\n\r\n                  <b>- A:</b> Anh bận lắm em.<br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Em biết là một Trưởng phòng nhân sự như anh sẽ rất bận rộn Chắc anh cũng đã nghe về ABC nên nên em muốn giới thiệu với anh về việc mua chứng khoán công ty này ở công ty em. Hy vọng có thể thu xếp một cuộc hẹn thuận tiện với anh nhất. Vào thứ 5 này em đi giao dịch với khách hàng ở gần công ty anh. Không biết anh tiện vào 9h sáng hay 3h chiều hơn ạ?<br>\r\n\r\n                  <b>- A: </b>Anh vẫn chưa thấy hấp dẫn lắm em ạ. Vì làm trong ngành nên anh thấy công ty ABC cũng bình thường. <br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Không biết anh đọc qua những thông tin tài chính mới nhất của ABC chưa ạ? Bên công ty em có những thông tin chắc chắn sẽ khiến anh thấy hứng thú. Em sẽ mang một số phân tích tài chính để anh xem qua vào ngày mai ạ. Không biết 9h sáng hay 3h chiều anh tiện giờ nào hơn?<br>\r\n\r\n                  <b>- A:</b> Vậy chiều mai 3h nhé, có gì liên lạc với anh trước khi tới.<br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Em cảm ơn anh. Hẹn gặp anh vào 3h chiều mai tại văn phòng của anh nhé. Chúc anh ngày làm việc hiệu quả. Em chào anh.', '2021-05-20 10:44:21', NULL),
(2, 'Kịch bản 1', '<b>- Telesale chứng khoán</b>: Xin lỗi đây có phải là số máy của anh A không ạ? <br>\r\n\r\n                  <b>- A:</b> Tôi là A nghe đây<br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Em chào anh. Em là B, gọi điện cho anh từ công ty C. Hiện tại, bên em có một số cổ phiếu ABC em nghĩ rằng anh sẽ không bỏ qua. Anh có thể dành cho em 3 phút chứ ạ?<br>\r\n\r\n                  <b>- A:</b> Anh không quan tâm em ơi<br>\r\n\r\n                  <b>- Telesale chứng khoán: </b>Đây là một cơ hội đầu tư tốt mà công ty em chỉ dành cho một số lượng khách hàng giới hạn. Anh có thể cho em thời gian để nói qua về cơ hội này với anh được không ạ?<br>\r\n\r\n                  <b>- A:</b> Anh bận lắm em.<br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Em biết là một Trưởng phòng nhân sự như anh sẽ rất bận rộn Chắc anh cũng đã nghe về ABC nên nên em muốn giới thiệu với anh về việc mua chứng khoán công ty này ở công ty em. Hy vọng có thể thu xếp một cuộc hẹn thuận tiện với anh nhất. Vào thứ 5 này em đi giao dịch với khách hàng ở gần công ty anh. Không biết anh tiện vào 9h sáng hay 3h chiều hơn ạ?<br>\r\n\r\n                  <b>- A: </b>Anh vẫn chưa thấy hấp dẫn lắm em ạ. Vì làm trong ngành nên anh thấy công ty ABC cũng bình thường. <br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Không biết anh đọc qua những thông tin tài chính mới nhất của ABC chưa ạ? Bên công ty em có những thông tin chắc chắn sẽ khiến anh thấy hứng thú. Em sẽ mang một số phân tích tài chính để anh xem qua vào ngày mai ạ. Không biết 9h sáng hay 3h chiều anh tiện giờ nào hơn?<br>\r\n\r\n                  <b>- A:</b> Vậy chiều mai 3h nhé, có gì liên lạc với anh trước khi tới.<br>\r\n\r\n                  <b>- Telesale chứng khoán:</b> Em cảm ơn anh. Hẹn gặp anh vào 3h chiều mai tại văn phòng của anh nhé. Chúc anh ngày làm việc hiệu quả. Em chào anh.', '2021-05-20 10:48:31', NULL),
(4, 'Kịch bản Sales mỹ phẩm', '<p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> Xin lỗi đây có phải số mấy của A không ạ?</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- A:</b> Mình A nghe đây.</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><b>- Telesale mỹ phẩm</b><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>:</b> Mình chào bạn. Mình là B gọi điện từ công ty X. Hiện tại bên mình là độc quyền phân phối chính thức của mỹ phẩm C tại Việt Nam. Bên mình đang có chương trình mua hàng đặc biệt dành riêng cho nhân viên văn phòng như bạn. Bạn có thể cho mình 3 phút được không ạ?</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- A:</b> Hiện tại mình không quan tâm và không có nhu cầu&nbsp;bạn.&nbsp;</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> Vì mỹ phẩm C đặc biệt nổi tiếng trong lĩnh vực cấp ẩm, cực kỳ phù hợp với những bạn nữ ngồi máy lạnh nhiều. Không biết sản phẩm cấp ẩm bạn đang sử dụng có hiệu quả không?</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- A:</b> Mình xài dòng này lâu rồi, rất phù hợp với da mình. Mình ngại thay đổi lắm bạn ạ.</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> À vậy da bạn khá nhạy cảm đúng không ạ? Mỹ phẩm C là dược mỹ phẩm nên cực kỳ lành tính. Bạn có thể yên tâm là rất ít kích ứng. Tuy nhiên công dụng phải sử dụng đều đặn thì mới thực sự hiệu quả. Không làm trắng nhanh như các loại mỹ phẩm mạnh được.&nbsp;</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- A:</b> Bạn nói rõ hơn về sản phẩm bên bạn được không? Mình cũng nghe về hãng C rồi nhưng chưa nhiều thông tin lắm. Đây là hãng mỹ phẩm cao cấp đúng không bạn.</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> Vâng, đúng vậy, mỹ phẩm C thuộc phân khúc cao cấp nên chất lượng rất tốt. Hiện tại mỹ phẩm C trên thị trường rất tràn lan, giá thì mỗi nơi mỗi kiểu, nhưng bên mình là nhà phân phối chính thức, nên bạn có thể yên tâm về chất lượng. C có khả năng giữ ẩm rất đỉnh. Sử dụng sẽ khiến da mướt, hơi ẩm lúc mới apply lên mặt nhưng sẽ đủ ẩm cả trong một ngày. Cảm giác đủ nước căng mọng như makeup kiểu Hàn Quốc ý bạn.&nbsp;</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><b>- A: </b><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\">Bên mình bán mỹ phẩm C như thế nào hả bạn?</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> Mỹ phẩm C có dưỡng ẩm là phù hợp nhất với bạn. Ngoài ra là một bộ bao gồm rất nhiều sản phẩm: toner, dưỡng ẩm, serum, kem mắt,... Nếu bạn dùng dưỡng ẩm nhưng về chiều vẫn đổ dầu thì chứng tỏ là da bạn vẫn đang thiếu ẩm. Dùng C sẽ không còn hiện tượng này nữa. Đặc biệt là sản phẩm là dược mỹ phẩm nên cực kỳ an toàn và lành tính. Bạn có thể tham khảo bảng thành phần của mỹ phẩm này…..</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- A:</b> Đúng là mặt mình cuối ngày đổ dầu rất ghê. Hóa ra mình vẫn bị thiếu ẩm. Vậy thì mình phải đổi mỹ phẩm quá. Mình nên xài cả bộ không bạn?</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> Da không đủ ẩm sẽ nhanh lão hóa nên bạn cứ thử sản phẩm nào phù hợp và tốt nhất rồi xài cố định luôn. Bạn có thể mua dưỡng ẩm dùng thử trước. Mình tin chắc bạn sẽ mua cả bộ sau đó luôn. Còn không bạn có thể mua cả bộ để trải nghiệm tốt nhất luôn.&nbsp;&nbsp;</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- A:</b> okay bạn. Mình đặt 1 bộ gửi về địa chỉ ABC nhé.&nbsp;</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif; color: rgb(33, 37, 41); font-size: 15px;\"><span style=\"font-size: 16px;\"><span style=\"font-family: Arial;\"><span style=\"color: rgb(0, 0, 0); font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif !important;\"><b>- Telesale mỹ phẩm:</b> Bên mình sẽ gửi sớm cho bạn nhé. Bạn cho mình thông tin về tên và số điện thoại nhé bạn.&nbsp;</span></span></span></p><p style=\"font-family: Muli, &quot;Helvetica Neue&quot;, sans-serif;\"><span style=\"font-size: 16px;\"><b>-A:</b>&nbsp;Oki bạn nhớ gửi sớm cho mình nhé để mình kiểm tra sản phẩm trước, nếu ưng thì mình sẽ lấy còn không thì mình sẽ hủy trả nhé. SĐT của mình là 034xxxx322</span></p>', '2021-05-21 02:48:21', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_number` int(10) DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `member_number`, `tag`, `created_at`, `updated_at`) VALUES
(1, 'Nhóm 1', NULL, 'Nhom1', '2021-05-20 07:08:41', NULL),
(5, 'Nhóm Sales', NULL, 'ádasda', '2021-05-21 08:06:35', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `survey`
--

CREATE TABLE `survey` (
  `id` bigint(20) NOT NULL,
  `survey_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `survey_type` int(11) NOT NULL COMMENT '1: 1 cau tra lơi, 2 nhieu cau tra loi, 3 danh gia',
  `survey_question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `survey`
--

INSERT INTO `survey` (`id`, `survey_name`, `survey_type`, `survey_question`, `created_at`) VALUES
(1, 'adasdasd', 1, 'ádasdasdasd', '2021-05-21 10:33:26'),
(2, 'sadasdasd', 2, 'ádasdasdasdasd', '2021-05-21 10:33:40'),
(3, 'Khảo sát số 1', 3, 'Bạn đánh giá về dịch vụ của chúng tôi như thế nào', '2021-05-21 10:33:43'),
(4, 'abc', 1, 'ádsda', '2021-05-21 10:33:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `survey_answer`
--

CREATE TABLE `survey_answer` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey_score_min` int(11) DEFAULT NULL,
  `survey_score_max` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`) VALUES
(2, 'InterITS', 'admin@gmail.com', NULL, '$2y$10$HQqdIbCmM9cvF39Mfya.c.jfxUMm6.MSl/roGO1f94Z1tYubvvhQy', NULL, '2021-05-18 02:22:30', '2021-05-18 02:22:30', 982699437),
(3, 'Test', 'oisonan1999@gmail.com', NULL, '$2y$10$T4dLOVJJ3cxXo1gUPZLdyOPJWwhOJwHb6wNMJOTCQeoBbFJ1MEsTS', NULL, '2021-05-18 03:01:24', '2021-05-18 03:01:24', 329985600),
(4, 'Test1', 'dbrr@gmail.com', NULL, '$2y$10$bR4DDt/2lfdG2DdX4kpLNe.OjzJW0VJGETOoQMDnewaNk71s0Gq3y', NULL, NULL, NULL, 123456789),
(5, 'Test2', 'Tesss@gmail.com', NULL, '$2y$10$zly0PqyZa4UoKZsTp89KpO60msbLam9RH8MlRGCnV23YCuDsvdbbO', NULL, NULL, NULL, 987654321),
(6, 'InterITS', 'vietlink@gmail.com', NULL, '$2y$10$quwHH/bo2KX2EpbmUto8z.s3XTm19Hqisz5KnHfjGrrQ3lTn0xmzO', NULL, NULL, NULL, 981234333);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `call_script`
--
ALTER TABLE `call_script`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `survey_answer`
--
ALTER TABLE `survey_answer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `agent`
--
ALTER TABLE `agent`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT cho bảng `call_script`
--
ALTER TABLE `call_script`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `survey`
--
ALTER TABLE `survey`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `survey_answer`
--
ALTER TABLE `survey_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

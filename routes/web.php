<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Check auth user
Route::get('logout', [\App\Http\Controllers\UserController::class, 'logout'])->name('logout');
Route::post('create', [\App\Http\Controllers\UserController::class, 'create'])->name('register');
Route::post('check', [\App\Http\Controllers\UserController::class, 'check'])->name('check');
//End check
//Admin
Route::get('adminLogin', [\App\Http\Controllers\AdminController::class, 'adminLogin']);
Route::get('adminLogout', [\App\Http\Controllers\AdminController::class, 'adminLogout'])->name('adminLogout');
Route::post('adminCheck', [\App\Http\Controllers\AdminController::class, 'adminCheck'])->name('adminCheck');
Route::get('adminHome', [\App\Http\Controllers\AdminController::class, 'adminHome']);
Route::get('adminHotline',[\App\Http\Controllers\AdminController::class,'adminHotline']);
Route::get('/edit/{id}',[\App\Http\Controllers\AdminController::class,'edit']);
Route::post('updateHotline',[\App\Http\Controllers\AdminController::class, 'updateHotline'])->name('updateHotline');
Route::post('addUser',[\App\Http\Controllers\AdminController::class,'addUser'])->name('addUser');
Route::post('updateUser',[\App\Http\Controllers\AdminController::class, 'updateUser'])->name('updateUser');
Route::get('deleteUser/{id}',[\App\Http\Controllers\AdminController::class,'deleteUser'])->name('deleteUser');
Route::get('adminAgent',[\App\Http\Controllers\AdminController::class,'adminAgent']);
Route::post('updateAdminAgent',[\App\Http\Controllers\AdminController::class,'updateAdminAgent'])->name('updateAdminAgent');
Route::get('/deleteAdminAgent/{id}',[\App\Http\Controllers\AdminController::class,'deleteAdminAgent'])->name('deleteAdminAgent');
//End Admin

Route::group(['middleware'=>['AuthCheck']], function(){
    //Trang chủ
    Route::get('home', [\App\Http\Controllers\UserController::class, 'home'])->name('home');
    //End trang chủ
    //Nhóm
    Route::post('update','App\Http\Controllers\NhomController@update')->name('update');
    Route::get('delete/{id}',[\App\Http\Controllers\NhomController::class,'delete'])->name('delete');
    Route::post('addGroup',[\App\Http\Controllers\NhomController::class, 'addGroup'])->name('addGroup');
    Route::get('group', [App\Http\Controllers\NhomController::class, 'group'])->name('group');
    //End nhóm
    //Đăng ký đăng nhập
    Route::get('profile/{id}', [\App\Http\Controllers\UserController::class,'profile'])->name('profile');
    Route::post('updateProfile',[\App\Http\Controllers\UserController::class,'updateProfile'])->name('updateProfile');
    Route::get('/',[\App\Http\Controllers\UserController::class,'login']);
    Route::get('register', [\App\Http\Controllers\UserController::class, 'register']);
    Route::get('forgetpassword', 'App\Http\Controllers\ForgetPasswordController@forgetpassword');
    Route::get('resetpassword', 'App\Http\Controllers\ForgetPasswordController@resetpassword');
    //End Đăng ký đăng nhập

    //Nhân viên
    Route::post('uploadAgent', [\App\Http\Controllers\NhanvienController::class,'uploadAgent'])->name('uploadAgent');
    Route::get('/agent/{id}', [\App\Http\Controllers\NhanvienController::class, 'showAgent'])->name('agent');
    Route::post('addAgent',[\App\Http\Controllers\NhanvienController::class, 'addAgent'])->name('addAgent');
    Route::post('updateAgent',[\App\Http\Controllers\NhanvienController::class, 'updateAgent'])->name('updateAgent');
    Route::get('/agent/deleteAgent/{id}',[\App\Http\Controllers\NhanvienController::class,'deleteAgent'])->name('deleteAgent');
    //End Nhân viên

    //Khảo sát
    Route::get('survey', [\App\Http\Controllers\KhaosatController::class, 'survey'])->name('survey');
    Route::post('addSurvey',[\App\Http\Controllers\KhaosatController::class,'addSurvey'])->name('addSurvey');
    Route::post('updateSurvey',[\App\Http\Controllers\KhaosatController::class,'updateSurvey'])->name('updateSurvey');
    Route::post('updateSurveyInfo',[\App\Http\Controllers\KhaosatController::class,'updateSurveyInfo'])->name('updateSurveyInfo');
    Route::get('deleteSurvey/{id}',[\App\Http\Controllers\KhaosatController::class,'deleteSurvey'])->name('deleteSurvey');
    //End khảo sát

    //Kịch bản
    Route::get('callscript', [\App\Http\Controllers\KichbanController::class, 'callscript'])->name('callscript');
    Route::get('editcallscript/{id}', 'App\Http\Controllers\KichbanController@editcallscript')->name('editcallscript');
    Route::post('updateCallScript',[\App\Http\Controllers\KichbanController::class, 'updateCallScript'])->name('updateCallScript');
    Route::get('addScript', [\App\Http\Controllers\KichbanController::class, 'addScript']);
    Route::post('addCallScript',[\App\Http\Controllers\KichbanController::class, 'addCallScript'])->name('addCallScript');
    Route::get('deleteScript/{id}',[\App\Http\Controllers\KichbanController::class,'deleteScript'])->name('deleteScript');
    //End Kịch bản

    //Khách hàng
    Route::get('customer', [\App\Http\Controllers\KhachhangController::class,'customer'])->name('customer');
    Route::post('uploadCustomer',[\App\Http\Controllers\KhachhangController::class,'uploadCustomer'])->name('uploadCustomer');
    Route::post('addCustomer',[\App\Http\Controllers\KhachhangController::class,'addCustomer'])->name('addCustomer');
    Route::post('updateCustomer',[\App\Http\Controllers\KhachhangController::class,'updateCustomer'])->name('updateCustomer');
    Route::get('deleteCustomer/{id}',[\App\Http\Controllers\KhachhangController::class,'deleteCustomer'])->name('deleteCustomer');
    //End Khách hàng

    //Chiến dịch
    Route::get('chiendich', 'App\Http\Controllers\ChiendichController@chiendich')->name('chiendich');
    Route::get('chitietchiendich', 'App\Http\Controllers\ChiendichController@chitietchiendich');
    Route::get('suachiendich', 'App\Http\Controllers\ChiendichController@suachiendich');
    Route::get('themchiendich', 'App\Http\Controllers\ChiendichController@themchiendich');
    //End Chiến dịch

    //Quay số
    Route::get('quayso', 'App\Http\Controllers\QuaysoController@quayso')->name('quayso');
    Route::get('ketquacuocgoi', 'App\Http\Controllers\QuaysoController@ketquacuocgoi')->name('ketquacuocgoi');
    //End quay số

    //Phiếu ghi
    Route::get('phieughi', 'App\Http\Controllers\PhieughiController@phieughi')->name('phieughi');
    //End phiếu ghi

    //Hotline
    Route::get('hotline', [\App\Http\Controllers\HotlineController::class,'hotline'])->name('hotline');
    Route::post('addHotline',[\App\Http\Controllers\HotlineController::class,'addHotline'])->name('addHotline');
    Route::post('editHotline',[\App\Http\Controllers\HotlineController::class,'editHotline'])->name('editHotline');
    Route::get('deleteHotline/{id}',[\App\Http\Controllers\HotlineController::class,'deleteHotline'])->name('deleteHotline');
});



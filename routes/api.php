<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('apiAuthUser',[\App\Http\Controllers\UserController::class,'apiAuthUser']);

Route::group(['middleware' => ['auth:sanctum']], function(){
    Route::get('apiGetUser',[\App\Http\Controllers\UserController::class,'apiGetUser'])->name('apiGetUser');
    Route::post('apiCreateUser', [\App\Http\Controllers\UserController::class, 'apiCreateUser'])->name('apiCreateUser');
    Route::post('apiHotlineUser', [\App\Http\Controllers\HotlineController::class, 'apiHotlineUser'])->name('apiHotlineUser');
    Route::post('apiCreateAgent',[\App\Http\Controllers\NhanvienController::class,'apiCreateAgent'])->name('apiCreateAgent');
});






//Modal thêm nhân viên
$('#Themnhanvien').on('shown.bs.modal', function () {
    $('#Themnhanvien').trigger('focus')
});
//End Modal thêm nhân viên

//Modal sửa nhân viên
$('#Suanhanvien').on('shown.bs.modal', function () {
    $('#Suanhanvien').trigger('focus')
});
//End sửa nhân viên

//Chức năng update nhân viên
$(document).on('click', '.edit', function()
{
    var _this = $(this).parents('tr');
    $('#id').val(_this.find('.id').text());
    $('#member_name').val(_this.find('.member_name').text());
    $('#member_address').val(_this.find('.member_address').text());
    $('#member_email').val(_this.find('.member_email').text());
    $('#member_phone').val(_this.find('.member_phone').text());
});
//End

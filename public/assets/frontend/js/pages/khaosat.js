//Modal thêm khảo sát
$('#Themkhaosat').on('shown.bs.modal', function () {
    $('#Themkhaosat').trigger('focus')
});
//End

//Modal sửa khảo sát
$('#Suakhaosat').on('shown.bs.modal', function () {
    $('#Suakhaosat').trigger('focus')
});
//End


//Khảo sát
$(document).ready(function () {

    $('#survey_type').on('change', function() {
        if( this.value === '1') {
            $('#surveyOneAnswer').show();
            $('#surveyMultiAnswer').hide();
            $('#surveyScore').hide();
        }

        if( this.value === '2') {
            $('#surveyOneAnswer').hide();
            $('#surveyMultiAnswer').show();
            $('#surveyScore').hide();
        }

        if( this.value === '3') {
            $('#surveyOneAnswer').hide();
            $('#surveyMultiAnswer').hide();
            $('#surveyScore').show();
        }
    });

    $('#edit_survey_type').on('change', function() {
        if( this.value === '1') {
            $('#e_surveyOneAnswer').show();
            $('#e_surveyMultiAnswer').hide();
            $('#e_surveyScore').hide();
        }

        if( this.value === '2') {
            $('#e_surveyOneAnswer').hide();
            $('#e_surveyMultiAnswer').show();
            $('#e_surveyScore').hide();
        }

        if( this.value === '3') {
            $('#e_surveyOneAnswer').hide();
            $('#e_surveyMultiAnswer').hide();
            $('#e_surveyScore').show();
        }
    });
});
//End Khảo sát


//Button thêm khảo sát
$(document).ready(function () {
    var counter = 2;
    $("#addButton").click(function () {
        if (counter > 10) {
            alert("Tối đa là 10 câu trả lời!");
            return false;
        }
        var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter).attr('class','form-group');
        newTextBoxDiv.after().html('<label> Nhập câu trả lời: </label>' + '<input class="form-control" type="text" name="survey_multi_answer[]" value="" >');
        newTextBoxDiv.appendTo("#TextBoxesGroup");
        counter++;
    });

    $("#removeButton").click(function () {
        if (counter == 1) {
            alert("Không còn câu trả lời để xóa, vui lòng thêm câu trả lời!");
            return false;
        }
        counter--;

        $("#TextBoxDiv" + counter).remove();
    });


    $("#getButtonValue").click(function () {
        var msg = '';
        for (i = 1; i < counter; i++) {
            msg += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
        }
        alert(msg);
    });
});
//end

//Button sửa khảo sát
$(document).ready(function () {
    var e_counter = 2;
    $("#e_addButton").click(function () {
        if (e_counter > 10) {
            alert("Tối đa là 10 câu trả lời!");
            return false;
        }
        var newTextBoxDiv = $(document.createElement('div')).attr("id", 'e_TextBoxDiv' + e_counter).attr('class','form-group');
        newTextBoxDiv.after().html('<label> Nhập câu trả lời: </label>' + '<input class="form-control" type="text" name="edit_survey_multi_answer[]" value="" >');
        newTextBoxDiv.appendTo("#e_surveyMultiAnswer");
        e_counter++;
    });

    $("#e_removeButton").click(function () {
        if (e_counter == 1) {
            alert("Không còn câu trả lời để xóa, vui lòng thêm câu trả lời!");
            return false;
        }
        e_counter--;

        $("#e_TextBoxDiv" + e_counter).remove();
    });

});
//end


//mapping du lieu khi sua khao sat
function editKhaoSat(id) {

    $('#updateSurveyInfoSurveyID').val(id);

    $.ajax({
        type: $('#updateSurveyInfoForm').attr('method'),
        url: $('#updateSurveyInfoForm').attr('action'),
        data: $('#updateSurveyInfoForm').serialize(),
        success: function (data) {
            if (data.result == true) {
                $('#edit_id').val(data.survey.id);
                $('#edit_survey_name').val(data.survey.survey_name);
                $("#edit_survey_type option").filter(function(){
                    return $(this).val() == data.survey.survey_type
                }).prop('selected', true);

                $('#edit_survey_question').val(data.survey.survey_question);

                //SHOW ANSWER
                if(data.survey.survey_type === 1) {
                    $('#e_surveyOneAnswer').show();
                    $('#e_surveyMultiAnswer').hide();
                    $('#e_surveyScore').hide();

                    $.each(data.surveyAnswer, function (i,v)
                    {
                        $('#edit_survey_answer').val(v.answer);
                    });
                }

                if( data.survey.survey_type === 2) {
                    $('#e_surveyOneAnswer').hide();
                    $('#e_surveyMultiAnswer').show();
                    $('#e_surveyScore').hide();

                    $.each(data.surveyAnswer, function (i,v)
                    {
                        var e_counter = 1;
                        var newTextBoxDiv = $(document.createElement('div')).attr("id", 'e_TextBoxDiv' + e_counter).attr('class','form-group');
                        newTextBoxDiv.after().html('<label> Nhập câu trả lời: </label>' + '<input class="form-control" type="text" name="edit_survey_multi_answer[]" value="'+v.answer+'" >');
                        newTextBoxDiv.appendTo("#e_surveyMultiAnswer");
                        e_counter ++;
                    });

                }

                if( data.survey.survey_type === 3) {
                    $('#e_surveyOneAnswer').hide();
                    $('#e_surveyMultiAnswer').hide();
                    $('#e_surveyScore').show();

                    $.each(data.surveyAnswer, function (i,v)
                    {
                        $('#edit_survey_score_min').val(v.survey_score_min);
                        $('#edit_survey_score_max').val(v.survey_score_max);
                    });
                }

                $('#Suakhaosat').modal('show');
            }else {
            }

        },
        error: function (err) {

        },
        complete: function () {
        }
    });


}
//End


//Modal sửa khách hàng
$('#Suakhachhang').on('shown.bs.modal', function () {
    $('#Suakhachhang').trigger('focus')
});
//End sửa khách hàng

//Chức năng update khách hàng
$(document).on('click', '.edit', function()
{
    var _this = $(this).parents('tr');
    $('#id').val(_this.find('.id').text());
    $('#name').val(_this.find('.name').text());
    $('#address').val(_this.find('.address').text());
    $('#phone').val(_this.find('.phone').text());

    for (i = 1; i <= _this.find('.tags').children().length; i++) {
        let childDiv =  _this.find('.tags').children("div:nth-child("+ i +")");
        $('#editTags').tagsinput('add', childDiv.text());
    }

});
//End

//Custom tag color
var colors = ['#58FAF4','orange', 'skyblue', 'yellow', 'plum', 'pink'];
var boxes = document.querySelectorAll(".badge");

for (i = 0; i < boxes.length; i++) {
    boxes[i].style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
}

//End

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Quản lý Agent</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>
<body>

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Quản lý <b>Agent</b></h2>
                </div>
                <div class="col-sm-6 text-right">
{{--                    <a href="#addAgentModal" class="btn btn-success" data-toggle="modal">--}}
{{--                        <span>Thêm mới Agent</span></a>--}}
                    <a href="{{url('/adminHome')}}" class="btn btn-info">
                        <span>Trang chủ Admin</span></a>
                </div>
            </div>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success" id="success-alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('fail'))
            <div class="alert alert-danger" id="danger-alert">
                {{ session()->get('fail') }}
            </div>
        @endif
        <div class="section-body">
            <div class="card">
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped table-md-12">
                            <tr>
                                <th>ID Agent</th>
                                <th>Họ và tên</th>
                                <th>Điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Email</th>
                                <th>Thao tác</th>
                            </tr>
                            @foreach($list as $item)
                                <tr>
                                    <td class="id">{{$item->id}}</td>
                                    <td class="member_name">{{$item->member_name}}</td>
                                    <td class="member_phone">{{'0'.$item->member_phone}}</td>
                                    <td class="member_address">{{$item->member_address}}</td>
                                    <td class="member_email">{{$item->member_email}}</td>
                                    <td>
                                        <a href="#editAgentModal" class="edit" data-toggle="modal"><i
                                                class="material-icons"
                                                data-toggle="tooltip"
                                                title="Sửa">&#xE254;</i></a>
                                        <a href="deleteAdminAgent/{{$item->id}}" onclick="return confirm('Bạn có muốn xóa?')"
                                           class="delete"><i
                                                class="material-icons"
                                                data-toggle="tooltip"
                                                title="Xóa">&#xE872;</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                {{$list->links()}}
            </div>
        </div>
    </section>
</div>


<!-- Edit Agent Modal HTML -->
<div id="editAgentModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa thông tin Agent</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="POST" action="{{route('updateAdminAgent')}}">
                {{csrf_field()}}
                <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value=""/>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Họ và tên</label>
                        <input type="text" name="member_name" id="member_name" class="form-control" required value="{{old('name')}}">
                        <span style="color: red;">@error('member_name'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="member_email" id="member_email" class="form-control" required value="">
                        <span style="color: red;">@error('member_email'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Điện thoại</label>
                        <input type="text" name="member_phone" id="member_phone" class="form-control" value="">
                        <span style="color: red;">@error('member_phone'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input type="text" name="member_address" id="member_address" class="form-control" value="">
                        <span style="color: red;">@error('member_address'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" name="member_password" class="form-control" required value="">
                        <span style="color: red;">@error('member_password'){{ $message }} @enderror</span>
                    </div>
                    <input type="hidden" class="form-control" value="{{ request()->id }}" name="member_group_id">
                </div>
                <div class="modal-footer">
                    <button type="submit" id="" name="" class="btn btn-primary">Sửa</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End -->

<script>
    $(document).ready(function () {
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();
    });
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#success-alert").slideUp(500);
    });
    $("#danger-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#danger-alert").slideUp(500);
    });
    $(document).on('click', '.edit', function () {
        var _this = $(this).parents('tr');
        $('#id').val(_this.find('.id').text());
        $('#member_name').val(_this.find('.member_name').text());
        $('#member_email').val(_this.find('.member_email').text());
        $('#member_phone').val(_this.find('.member_phone').text());
        $('#member_address').val(_this.find('.member_address').text());
        $('#member_password').val(_this.find('.member_password').text());
    });
</script>
</body>
</html>


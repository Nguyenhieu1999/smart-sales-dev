<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Thông tin Hotline</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/selectric/public/selectric.css')}}">
    <script src="{{asset('assets/frontend/node_modules/selectric/public/jquery.selectric.min.js')}}"></script>
    <style>

        .form-control:focus {
            box-shadow: none;
            border-color: #BA68C8
        }

        .profile-button {
            background: rgb(99, 39, 120);
            box-shadow: none;
            border: none
        }

        .profile-button:hover {
            background: #682773
        }

        .profile-button:focus {
            background: #682773;
            box-shadow: none
        }

        .profile-button:active {
            background: #682773;
            box-shadow: none
        }

        .back:hover {
            color: #682773;
            cursor: pointer
        }

        .labels {
            font-size: 11px
        }

        .add-experience:hover {
            background: #BA68C8;
            color: #fff;
            cursor: pointer;
            border: solid 1px #BA68C8
        }
    </style>
</head>
<body>
<div class="container rounded bg-white mt-5 mb-5">
    @if(session()->has('success'))
        <div class="alert alert-success" id="success-alert">
            {{ session()->get('success') }}
        </div>
    @endif
    @if(session()->has('fail'))
        <div class="alert alert-danger" id="danger-alert">
            {{ session()->get('fail') }}
        </div>
    @endif
    <form action="{{route('updateHotline')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value="{{$Info->id}}"/>
        <div class="row">
            <div class="col-md-6 border-right">
                <div class="d-flex flex-column align-items-left text-left"><img class="mt-5"
                                                                                src="{{asset('uploads/contract_image/'.$Info->contract_image)}}"
                                                                                width="500px;" height="600px;"
                                                                                alt="Image">
                    <div class="form-group">
                        <label>Ảnh hợp đồng</label>
                        <div class="fallback">
                            <input name="contract_image" type="file" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h3 class="text-right">Sửa thông tin hotline</h3>
                    </div>
                    <div class="form-group">
                        <label>Số hotline</label>
                        <input type="text" class="form-control" name="hotline_number" value="{{$Info->hotline_number}}">
                    </div>

                    <div class="form-group">
                        <label>Chọn nhà mạng cung cấp</label>
                        <select class="form-control selectric" name="telecom_operator">
                            <option>Viettel</option>
                            <option>Vinaphone</option>
                            <option>Mobiphone</option>
                        </select>
                        <span style="color: red;">@error('telecom_operator'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <select class="form-control selectric" name="status">
                            <option value="{{$Info->status = 1}}">Đã xác thực</option>
                            <option value="{{$Info->status = 0}}">Chưa xác thực</option>
                        </select>
                        <span style="color: red;">@error('status'){{ $message }} @enderror</span>
                    </div>

                    <div class="mt-5 text-center">
                        <button class="btn btn-primary profile-button" type="submit">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>


<script>
    $(function () {
        $('select').selectric();
    });
    $(document).ready(function () {
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();


    });
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#success-alert").slideUp(500);
    });

    $("#danger-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#danger-alert").slideUp(500);
    });

    $(document).on('click', '.edit', function () {

        var _this = $(this).parents('tr');
        $('#id').val(_this.find('.id').text());
        $('#name').val(_this.find('.name').text());
        $('#email').val(_this.find('.email').text());
        $('#phone').val(_this.find('.phone').text());
        $('#taxcode').val(_this.find('.taxcode').text());
        $('#job').val(_this.find('.job').text());
        $('#address').val(_this.find('.address').text());
    });
</script>

</body>
</html>


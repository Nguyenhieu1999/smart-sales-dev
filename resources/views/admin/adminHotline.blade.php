<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Quản lý Hotline</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>
<body>

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Quản lý <b>Hotline</b></h2>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{url('/adminHome')}}" class="btn btn-info">
                        <span>Trang chủ Admin</span></a>
                </div>
            </div>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success" id="success-alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('fail'))
            <div class="alert alert-danger" id="danger-alert">
                {{ session()->get('fail') }}
            </div>
        @endif
        <div class="section-body">
            <div class="card">
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped table-md-12">
                            <tr>
                                <th>ID</th>
                                <th>Số hotline</th>
                                <th>Tên nhà mạng</th>
                                <th>Ảnh hợp đồng</th>
                                <th>Ngày cập nhật</th>
                                <th>Trạng thái</th>
                                <th>Thao tác</th>
                            </tr>
                            @foreach($list as $item)
                                <tr>
                                    <td class="id">{{$item->id}}</td>
                                    <td class="hotline_number">{{'0'.$item->hotline_number}}</td>
                                    <td class="telecom_operator">{{$item->telecom_operator}}</td>
                                    <td><img src="{{asset('uploads/contract_image/'.$item->contract_image)}}"
                                             width="100px;" height="150px;" alt="Image"></td>
                                    <td>{{$item->created_at}}</td>
                                    @if($item -> status == '0')
                                        <td>Chưa xác thực</td>
                                    @elseif($item -> status == '1')
                                        <td>Đã xác thực</td>
                                    @endif
                                    <td>
                                        <a href="/edit/{{$item->id}}" class="edit"><i
                                                class="material-icons"
                                                data-toggle="tooltip"
                                                title="Sửa">&#xE254;</i></a>
                                        <a href="deleteUser/{{$item->id}}" onclick="return confirm('Bạn có muốn xóa?')"
                                           class="delete"><i
                                                class="material-icons"
                                                data-toggle="tooltip"
                                                title="Xóa">&#xE872;</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                {{$list->links()}}
            </div>
        </div>
    </section>
</div>


<script >
    $(document).ready(function () {
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();


    });
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#success-alert").slideUp(500);
    });

    $("#danger-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#danger-alert").slideUp(500);
    });

    $(document).on('click', '.edit', function () {

        var _this = $(this).parents('tr');
        $('#id').val(_this.find('.id').text());
        $('#name').val(_this.find('.name').text());
        $('#email').val(_this.find('.email').text());
        $('#phone').val(_this.find('.phone').text());
        $('#taxcode').val(_this.find('.taxcode').text());
        $('#job').val(_this.find('.job').text());
        $('#address').val(_this.find('.address').text());
    });
</script>

</body>
</html>


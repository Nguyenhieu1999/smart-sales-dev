<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>#</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>
<body>

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Quản lý <b>Doanh Nghiệp</b></h2>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{url('/adminHotline')}}" class="btn btn-info">
                        <span>Quản lý hotline</span></a>
                    <a href="{{url('/adminAgent')}}" class="btn btn-warning">
                        <span>Quản lý Agent</span></a>
                    <a href="#addUserModal" class="btn btn-success" data-toggle="modal">
                        <span>Thêm mới doanh nghiệp</span></a>
                    <a href="{{url('adminLogout')}}" class="btn btn-danger"><span>Đăng xuất</span></a>
                </div>
            </div>
        </div>
        @if(session()->has('success'))
            <div class="alert alert-success" id="success-alert">
                {{ session()->get('success') }}
            </div>
        @endif
        @if(session()->has('fail'))
            <div class="alert alert-danger" id="danger-alert">
                {{ session()->get('fail') }}
            </div>
        @endif
        <div class="section-body">
            <div class="card">
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped table-md-12">
                            <tr>
                                <th>ID Doanh nghiệp</th>
                                <th>Tên doanh nghiệp</th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Mã số thuế</th>
                                <th>Nghề nghiệp</th>
                                <th>Địa chỉ</th>
                                <th>Thao tác</th>
                            </tr>
                            @foreach($list as $item)
                                <tr>
                                    <td class="id">{{$item->id}}</td>
                                    <td class="name">{{$item->name}}</td>
                                    <td class="email">{{$item->email}}</td>
                                    <td class="phone">{{$item->phone}}</td>
                                    <td class="taxcode">{{$item->taxcode}}</td>
                                    <td class="job">{{$item->job}}</td>
                                    <td class="address">{{$item->address}}</td>
                                    <td>
                                        <a href="#editUserModal" class="edit" data-toggle="modal"
                                           data-idUpdate="'$value->id'"><i
                                                class="material-icons"
                                                data-toggle="tooltip"
                                                title="Sửa">&#xE254;</i></a>
                                        <a href="deleteUser/{{$item->id}}" onclick="return confirm('Bạn có muốn xóa?')"
                                           class="delete"><i
                                                class="material-icons"
                                                data-toggle="tooltip"
                                                title="Xóa">&#xE872;</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                {{$list->links()}}
            </div>
        </div>
    </section>
</div>

<!-- Add User Modal HTML -->
<div id="addUserModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="{{route('addUser')}}">
                {{csrf_field()}}
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới doanh nghiệp</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" value="">&times;
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tên</label>
                        <input type="text" name="name" class="form-control" required>
                        <span style="color: red;">@error('name'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" required value="">
                        <span style="color: red;">@error('email'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Điện thoại</label>
                        <input type="text" name="phone" class="form-control" value="">
                        <span style="color: red;">@error('phone'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Mã số thuế</label>
                        <input type="text" name="taxcode" class="form-control" value="">
                        <span style="color: red;">@error('taxcode'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input type="text" name="address" class="form-control" value="">
                        <span style="color: red;">@error('address'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Nghề nghiệp</label>
                        <input type="text" name="job" class="form-control">
                        <span style="color: red;">@error('job'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" name="password" class="form-control" required value="">
                        <span style="color: red;">@error('password'){{ $message }} @enderror</span>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tạo user</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End -->

<!-- Edit User Modal HTML -->
<div id="editUserModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sửa thông tin doanh nghiệp</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="POST" action="{{route('updateUser')}}">
                {{csrf_field()}}
                <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value=""/>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tên</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                        <span style="color: red;">@error('name'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" id="email" class="form-control" required>
                        <span style="color: red;">@error('email'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Điện thoại</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                        <span style="color: red;">@error('phone'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Mã số thuế</label>
                        <input type="text" name="taxcode" id="taxcode" class="form-control">
                        <span style="color: red;">@error('taxcode'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ</label>
                        <input type="text" name="address" id="address" class="form-control">
                        <span style="color: red;">@error('address'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Nghề nghiệp</label>
                        <input type="text" name="job" id="job" class="form-control">
                        <span style="color: red;">@error('job'){{ $message }} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu</label>
                        <input type="password" name="password" id="password" class="form-control" required>
                        <span style="color: red;">@error('password'){{ $message }} @enderror</span>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="" name="" class="btn btn-primary">Sửa</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End User Modal HTML -->



<script >
    $(document).ready(function () {
        // Activate tooltip
        $('[data-toggle="tooltip"]').tooltip();


    });
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#success-alert").slideUp(500);
    });

    $("#danger-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#danger-alert").slideUp(500);
    });

    $(document).on('click', '.edit', function () {

        var _this = $(this).parents('tr');
        $('#id').val(_this.find('.id').text());
        $('#name').val(_this.find('.name').text());
        $('#email').val(_this.find('.email').text());
        $('#phone').val(_this.find('.phone').text());
        $('#taxcode').val(_this.find('.taxcode').text());
        $('#job').val(_this.find('.job').text());
        $('#address').val(_this.find('.address').text());
    });
</script>

</body>
</html>

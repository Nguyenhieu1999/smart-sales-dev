@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Chiến dịch số 1</h1>
      <div class="section-header-breadcrumb">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#chitietcuocgoi"><i class="fa fa-cog"></i> Cấu hình</button>
      </div>
    </div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="card-body statistics-body">
              <div class="row">
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-info mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 9px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-graph-up" viewBox="0 0 16 16">
                          <path fill-rule="evenodd" d="M0 0h1v15h15v1H0V0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">100</h4>
                      <p class="card-text font-small-3 mb-0">Tổng cuộc gọi</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-warning mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                          <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">0</h4>
                      <p class="card-text font-small-3 mb-0">Tổng khách Big Data</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-danger mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-box" viewBox="0 0 16 16">
                          <path d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5 8 5.961 14.154 3.5 8.186 1.113zM15 4.239l-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923l6.5 2.6zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464L7.443.184z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">50</h4>
                      <p class="card-text font-small-3 mb-0">Tổng khách nội bộ</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-success  mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-outbound" viewBox="0 0 16 16">
                          <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">85</h4>
                      <p class="card-text font-small-3 mb-0">Tổng cuộc gọi ra</p>
                    </div>
                  </div>
                </div>


                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-info mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-inbound" viewBox="0 0 16 16">
                          <path d="M15.854.146a.5.5 0 0 1 0 .708L11.707 5H14.5a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793L15.146.146a.5.5 0 0 1 .708 0zm-12.2 1.182a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">0</h4>
                      <p class="card-text font-small-3 mb-0">Tổng cuộc gọi vào</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-warning mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-x" viewBox="0 0 16 16">
                          <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                          <path fill-rule="evenodd" d="M11.146 1.646a.5.5 0 0 1 .708 0L13 2.793l1.146-1.147a.5.5 0 0 1 .708.708L13.707 3.5l1.147 1.146a.5.5 0 0 1-.708.708L13 4.207l-1.146 1.147a.5.5 0 0 1-.708-.708L12.293 3.5l-1.147-1.146a.5.5 0 0 1 0-.708z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">15</h4>
                      <p class="card-text font-small-3 mb-0">Tổng không liên lạc dược</p>
                    </div>
                  </div>
                </div>



                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-danger mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
                          <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">15</h4>
                      <p class="card-text font-small-3 mb-0">Tổng cuộc chưa gọi</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-success mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-minus" viewBox="0 0 16 16">
                          <path fill-rule="evenodd" d="M10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z" />
                          <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">10</h4>
                      <p class="card-text font-small-3 mb-0">Tổng cuộc gọi nhỡ</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-info mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-file-earmark-text" viewBox="0 0 16 16">
                          <path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z" />
                          <path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">300</h4>
                      <p class="card-text font-small-3 mb-0">Tổng phiếu ghi</p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
                  <div class="media">
                    <div class="avatar bg-warning mr-2">
                      <div class="avatar-content">
                        <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cloud-lightning" viewBox="0 0 16 16">
                          <path d="M13.405 4.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 10H13a3 3 0 0 0 .405-5.973zM8.5 1a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 1zM7.053 11.276A.5.5 0 0 1 7.5 11h1a.5.5 0 0 1 .474.658l-.28.842H9.5a.5.5 0 0 1 .39.812l-2 2.5a.5.5 0 0 1-.875-.433L7.36 14H6.5a.5.5 0 0 1-.447-.724l1-2z" />
                        </svg>
                      </div>
                    </div>
                    <div class="media-body my-auto">
                      <h4 class="font-weight-bolder mb-0">100</h4>
                      <p class="card-text font-small-3 mb-0">Tổng chiến dịch gọi</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4>Danh sách cuộc gọi</h4>
            </div>
            <div class="card-body p-0">
              <div class="table-responsive">
                <table class="table table-striped table-md-12">
                  <tr>
                    <th>Thuê bao</th>
                    <th>Nhân viên</th>
                    <th>Đầu số gọi ra</th>
                    <th>Ngày gọi</th>
                    <th>Thời lượng</th>
                    <th>Trạng thái</th>
                    <th>Chi tiết</th>
                  </tr>
                  <tr>
                    <td>098xxxx437</td>
                    <td>Thangth</td>
                    <td>0366568956</td>
                    <td>12-05-2021 11:13:58</td>
                    <td>00:01:10</td>
                    <td>
                      <div class="badge badge-success">Thành công</div>
                    </td>
                    <td>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-search-plus"></i> </button>
                    </td>
                  </tr>
                  <tr>
                    <td>098xxxx437</td>
                    <td>Thangth</td>
                    <td>0366568956</td>
                    <td>12-05-2021 11:13:58</td>
                    <td>00:01:10</td>
                    <td>
                      <div class="badge badge-info">Không nghe máy</div>
                    </td>
                    <td>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-search-plus"></i> </button>
                    </td>
                  </tr>
                  <tr>
                    <td>098xxxx437</td>
                    <td>Thangth</td>
                    <td>0366568956</td>
                    <td>12-05-2021 11:13:58</td>
                    <td>00:01:10</td>
                    <td>
                      <div class="badge badge-warning">Cúp máy giữa chừng</div>
                    </td>
                    <td>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-search-plus"></i> </button>
                    </td>
                  </tr>
                  <tr>
                    <td>098xxxx437</td>
                    <td>Thangth</td>
                    <td>0366568956</td>
                    <td>12-05-2021 11:13:58</td>
                    <td>00:01:10</td>
                    <td>
                      <div class="badge badge-danger">Không liên lạc được</div>
                    </td>
                    <td>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-search-plus"></i> </button>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
</div>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cuộc gọi: #20210504145438</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="    background-color: #6777ef;">
      <div class="row">
            <div class="col-4">
       
            <p>Mã cuộc gọi: 20210504145438</p>

        <p>Gọi ra lúc: 12-05-2021 11:13:58</p>

        <p>Thời lượng cuộc gọi: 00:01:10</p>


        <p>Trạng thái: Khách hàng cúp máy giữa chừng</p>
      
            </div>

            <div class="col-8">
       
            <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home1" aria-selected="true">Ghi âm và nội dung</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile1" aria-selected="false">Trả lời khảo sát</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
            <audio id="audio-player" controls="" title="Audio" style="width: 100%" __idm_id__="752479233">
              <source src="http://103.141.140.189:19991/f/b6a0d15a9cf87c6cef402e5131f24f6b/bot/6004567f72ca0738aae82ea8/291336cf3afc3e782d6113d7a29dd770/dc0fe61e2f7538ef696f8bcc6edb747a.mp3" type="audio/mpeg">
            </audio>
            <div id="message_display" class="col-12" style="height: 500px; border-style: solid; border-width: 1px; overflow-y: scroll; width: 100%; margin-bottom: 20px">
              <div class="msg_bbl bot">
                <a href="#" class="playAudio" start_time="0"><i class="fa fa-play-circle"></i></a>
                <b>Bot (00:00:00):</b>em chào anh, em là Callbot tự động, em gọi đến từ ngân hàng Seabank, xin hỏi đây có phải số máy của anh Quyền không vậy
              </div>
              <div class="msg_bbl user">
                <a href="#" class="playAudio" start_time="0"><i class="fa fa-play-circle"></i></a>
                <b>User (00:00:00):</b>alo
              </div>
              <div class="msg_bbl user">
                <a href="#" class="playAudio" start_time="9"><i class="fa fa-play-circle"></i></a>
                <b>User (00:00:09):</b>đúng rồi
              </div>
              <div class="msg_bbl bot">
                <a href="#" class="playAudio" start_time="13"><i class="fa fa-play-circle"></i></a>
                <b>Bot (00:00:13):</b>Dạ thưa anh, em gọi để tư vấn cho anh chương trình ưu đãi đặc biệt giữa Seabank và VNPT, anh được cấp 1 chiếc thẻ tín dụng có sẵn hạn mức 100 triệu, để có thể dễ dàng chi tiêu mua sắm với các chương trình ưu đãi hấp dẫn từ Seabank. Không biết anh có đồng ý mở thẻ không
              </div>
              <div class="msg_bbl user">
                <a href="#" class="playAudio" start_time="27"><i class="fa fa-play-circle"></i></a>
                <b>User (00:00:27):</b>anh không
              </div>
              <div class="msg_bbl bot">
                <a href="#" class="playAudio" start_time="32"><i class="fa fa-play-circle"></i></a>
                <b>Bot (00:00:32):</b>Dạ xin lỗi anh, không biết anh đã có thẻ Seabank chưa, hay đang sử dụng thẻ tín dụng của một ngân hàng khác vậy
              </div>
              <div class="msg_bbl user">
                <a href="#" class="playAudio" start_time="37"><i class="fa fa-play-circle"></i></a>
                <b>User (00:00:37):</b>có rồi
              </div>
              <div class="msg_bbl bot">
                <a href="#" class="playAudio" start_time="41"><i class="fa fa-play-circle"></i></a>
                <b>Bot (00:00:41):</b>dạ khi đăng ký mở thẻ tín dụng này, anh không cần cung cấp các hồ sơ để chứng minh năng lực tài chính, với thủ tục khá đơn giản. hạn mức đề xuất bên em mời anh tạm dùng là 100 triệu để trải nghiệm thêm ưu đãi thẻ mà những ngân hàng khác không có được. vậy nên em vẫn muốn mời anh sử dụng thẻ tín dụng này, không biết anh có đồng ý mở thẻ không vậy
              </div>
              <div class="msg_bbl user">
                <a href="#" class="playAudio" start_time="59"><i class="fa fa-play-circle"></i></a>
                <b>User (00:00:59):</b>anh không
              </div>
              <div class="msg_bbl bot">
                <a href="#" class="playAudio" start_time="64"><i class="fa fa-play-circle"></i></a>
                <b>Bot (00:01:04):</b>Dạ cảm ơn anh đã lắng nghe, em xin phép kết thúc cuộc gọi, chúc anh một ngày tốt lành
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
            <div class="table-responsive">
              <table class="table table-striped table-md">
                <tr>
                  <th><strong>Câu hỏi</strong></th>
                  <td>Chúng tôi có hiểu biết và hữu ích cho bạn không?</td>
                </tr>
                <tr>
                  <th><strong>Trả lời</strong></th>
                  <td>
                    Có
                  </td>
                </tr>
                <tr>
                  <th><strong>Câu hỏi</strong></th>
                  <td>Chúng tôi phục vụ có nhanh chóng không?</td>
                </tr>
                <tr>
                  <th><strong>Trả lời</strong></th>
                  <td>Không</td>
                </tr>
                <tr>
                  <th><strong>Câu hỏi</strong></th>
                  <td>Bạn còn có góp ý nào cho chúng tôi không</td>
                </tr>
                <tr>
                  <th><strong>Trả lời</strong></th>
                  <td>Không</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
            </div>
      </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chitietcuocgoi" tabindex="-1" role="dialog" aria-labelledby="chitietcuocgoiTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Chi tiết chiến dịch</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body p-0">
          <div class="table-responsive">
            <table class="table table-striped table-md">
              <tr>
                <th><strong>Tên chiến dịch</strong></th>
                <td>Chiến dịch số 1</td>
              </tr>
              <tr>
                <th><strong>Trạng thái</strong></th>
                <td>
                  <div class="badge badge-info">Đang chạy</div>
                </td>
              </tr>
              <tr>
                <th><strong>Danh sách thuê bao</strong></th>
                <td>Test_contact (100 thuê bao)</td>
              </tr>
              <tr>
                <th><strong>Gọi lại cho mỗi thuê bao</strong></th>
                <td>3</td>
              </tr>
              <tr>
                <th><strong>Gọi lại sau (Giờ)</strong></th>
                <td>1h</td>
              </tr>
              <tr>
                <th><strong>Nhóm chỉ định</strong></th>
                <td>Sale team</td>
              </tr>
              <tr>
                <th><strong>Kịch bản cuộc gọi</strong></th>
                <td>Kịch bản Sale</td>
              </tr>
              <tr>
                <th><strong>Khảo sát</strong></th>
                <td>Khảo sát số 1</td>
              </tr>
              <tr>
                <th><strong>ID người gọi</strong></th>
                <td>123213</td>
              </tr>
              <tr>
                <th><strong>Thời gian chờ</strong></th>
                <td>30s</td>
              </tr>
              <tr>
                <th><strong>Thực hiện cuộc gọi tiếp theo</strong></th>
                <td>5s</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('js')

@endpush
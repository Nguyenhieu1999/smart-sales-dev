@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Import file khách hàng</h1>
    </div>
    <div class="section-body">
    
      <div class="card">
        <div class="card-body">
          <h4>Tải file lên</h4>

          Yêu cầu định dạng file là xlsx, bạn có thể tải file mẫu <a href="#">tại
          đây</a>.
          <form action="#" class="dropzone" id="mydropzone">
            <div class="fallback">
              <input name="file" type="file" multiple />
            </div>
          </form>
          <br />
          <br />
          <br />
          <h4 style="font-weight: bold;">Gắn tag</h4>
          <div class="form-group">
            <input type="text" class="form-control inputtags">
          </div>
          <br />
          <br />
          <br />
        </div>
      </div>
      <div class="card-body text-right">
        <button type="submit" class="btn btn-primary">Tải lên</button>
      </div>
    </div>
  </section>
</div>
@endsection

@push('js')

@endpush
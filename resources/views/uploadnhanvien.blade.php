@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Import file nhân viên</h1>
    </div>
    <div class="section-body">
      <p class="section-lead">
        Yêu cầu định dạng file là xlsx, bạn có thể tải file mẫu <a href="#">tại
          đây</a>.
      </p>
      <div class="card">
        <div class="card-body">
          <h4 style="font-weight: bold;">Tải file lên</h4>
          <form action="#" class="dropzone" id="mydropzone">
            <div class="fallback">
              <input name="file" type="file" multiple />
            </div>
          </form>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </div>
      </div>
      <div class="card-body text-right">
        <button type="submit" class="btn btn-primary">Tải lên</button>
      </div>
    </div>
  </section>
</div>
@endsection

@push('js')

@endpush
@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Hồ sơ</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Xin chào {{ Session::get('LoggedUser') ->name}}!</h2>
                <p class="section-lead">
                    Thay đổi thông tin doanh nghiệp
                </p>
                @if(session()->has('success'))
                    <div class="alert alert-success" id="success-alert">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('fail'))
                    <div class="alert alert-danger" id="danger-alert">
                        {{ session()->get('fail') }}
                    </div>
                @endif
                <div class="row mt-sm-4">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{route('updateProfile')}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="cid" id="cid" value="{{$Info->id}}">
                                <div class="card-header">
                                    <h4>Sửa thông tin doanh nghiệp</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-12">
                                            <label>Tên doanh nghiệp</label>
                                            <input type="text" class="form-control" name="name" id="name"
                                                   value="{{$Info->name}}" required="">
                                            <span class="text-danger">@error('name'){{$message}} @enderror</span>
                                            <div class="invalid-feedback">
                                                Trường này không được bỏ trống
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Email doanh nghiệp</label>
                                            <input type="email" class="form-control" name="email" id="email"
                                                   value="{{$Info->email}}" required="">
                                            <span class="text-danger">@error('email'){{$message}} @enderror</span>
                                            <div class="invalid-feedback">
                                                Trường này không được bỏ trống
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-12">
                                            <label>Nghề nghiệp</label>
                                            <input type="text" class="form-control" name="job" id="job"
                                                   value="{{$Info->job}}" required="">
                                            <span class="text-danger">@error('job'){{$message}} @enderror</span>
                                            <div class="invalid-feedback">
                                                Trường này không được bỏ trống
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Điện thoại</label>
                                            <input type="tel" class="form-control" name="phone" id="phone"
                                                   value="{{$Info->phone}}">
                                            <span class="text-danger">@error('phone'){{$message}} @enderror</span>
                                            <div class="invalid-feedback">
                                                Trường này không được bỏ trống
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-12">
                                            <label>Mã số thuế</label>
                                            <input type="text" class="form-control" name="taxcode" id="taxcode"
                                                   value="{{$Info->taxcode}}" required="">
                                            <span class="text-danger">@error('taxcode'){{$message}} @enderror</span>
                                            <div class="invalid-feedback">
                                                Trường này không được bỏ trống
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Địa chỉ</label>
                                            <input type="text" class="form-control" name="address" id="address"
                                                   value="{{$Info->address}}">
                                            <span class="text-danger">@error('address'){{$message}} @enderror</span>
                                            <div class="invalid-feedback">
                                                Trường này không được bỏ trống
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('js')

@endpush

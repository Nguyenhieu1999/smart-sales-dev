<div class="main-sidebar" tabindex="1" style="overflow: hidden; outline: none;">
    @if(Session::get('LoggedUser') -> role_id == 1)
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="{{route('home',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}">Smart Sales</a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="{{route('home',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}">SS</a>
            </div>
            <ul class="sidebar-menu">
                <li class="nav-item">
                    <a href="{{route('home',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Tổng quan"><i
                            class="fas fa-home"></i><span>Tổng quan</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('customer',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Khách hàng"><i
                            class="fas fa-users"></i><span>Khách hàng</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('group',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Nhân viên"><i
                            class="fas fa-user-circle"></i><span>Nhân viên</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('callscript',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Kịch bản gọi"><i
                            class="fas fa-file-alt"></i><span>Kịch bản gọi</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('survey',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Khảo sát"><i
                            class="fas fa-poll-h"></i><span>Khảo sát</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('chiendich',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Chiến dịch"><i
                            class="fas fa-bolt"></i><span>Chiến dịch</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('quayso',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Quay số"><i
                            class="fas fa-phone-square"></i><span>Quay số</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('phieughi',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Phiếu ghi"><i
                            class="fas fa-tasks"></i><span>Phiếu ghi</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{route('hotline',[Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}" class="nav-link" data-toggle="tooltip" title="Cấu hình"><i class="fas fa-cog"></i><span>Cấu hình</span></a>
                </li>
            </ul>
        </aside>
    @elseif(Session::get('LoggedAgent') -> role_id == 2)
        <aside id="sidebar-wrapper">
            <div class="sidebar-brand">
                <a href="{{url('/home')}}">Smart Sales</a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="{{url('/home')}}">S</a>
            </div>
            <ul class="sidebar-menu">
                <li class="nav-item">
                    <a href="{{url('/quayso')}}" class="nav-link" data-toggle="tooltip" title="Quay số"><i
                            class="fas fa-phone-square"></i><span>Quay số</span></a>
                </li>

            </ul>
        </aside>
    @endif
</div>

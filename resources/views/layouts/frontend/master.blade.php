<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Smart Saler</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap-icons.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/smart_wizard.min.css" rel="stylesheet" type="text/css" />
  <link href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/smart_wizard_theme_dots.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/jqvmap/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/weathericons/css/weather-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/weathericons/css/weather-icons-wind.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/summernote/dist/summernote-bs4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/bootstrap-social/bootstrap-social.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/bootstrap-daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/selectric/public/selectric.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/dropzone/dist/min/dropzone.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/owl.carousel/dist/assets/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/frontend/node_modules/izitoast/dist/css/iziToast.min.css')}}">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/css/components.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/css/chitietcuocgoi.css')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/css/bd-wizard.css')}}">
</head>

<!--  class="sidebar-mini" -->

<body class="sidebar-mini">
  <div id="app">
    <div class="main-wrapper">
      @include('layouts.frontend.partial.header')
      @include('layouts.frontend.partial.sidebar')
      <!-- Main Content -->
      @yield('content')
      @include('layouts.frontend.partial.footer')
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/jquery.smartWizard.min.js"></script>
  <script src="{{asset('assets/frontend//js/stisla.js')}}"></script>

  <!-- JS Libraies -->
  <script src="{{asset('assets/frontend/node_modules/simpleweather/jquery.simpleWeather.min.js')}}"></script>

  <script src="{{asset('assets/frontend/node_modules/jquery-pwstrength/jquery.pwstrength.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/chart.js/dist/Chart.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/summernote/dist/summernote-bs4.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/cleave.js/dist/cleave.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/cleave.js/dist/addons/cleave-phone.us.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/jquery-pwstrength/jquery.pwstrength.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/select2/dist/js/select2.full.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/selectric/public/jquery.selectric.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/dropzone/dist/min/dropzone.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/owl.carousel/dist/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/jqvmap/dist/maps/jquery.vmap.indonesia.js')}}"></script>
  <script src="{{asset('assets/frontend/node_modules/izitoast/dist/js/iziToast.min.js')}}"></script>

  <!-- Template JS File -->
  <script src="{{asset('assets/frontend/js/scripts.js')}}"></script>
  <script src="{{asset('assets/frontend/js/custom.js')}}"></script>
  <script src="{{asset('assets/frontend/js/myjs.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/chiendich.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/nhanvien.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/nhom.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/khachhang.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/khaosat.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/thongke.js')}}"></script>
  <script src="{{asset('assets/frontend/js/pages/hotline.js')}}"></script>
  <script src="{{asset('assets/frontend/js/jquery.steps.min.js')}}"></script>
  <script src="{{asset('assets/frontend/js/bd-wizard.js')}}"></script>
  <!-- <script src="{{asset('assets/frontend/js/page/components-statistic.js')}}"></script> -->


  <!-- Page Specific JS File -->
  <script src="{{asset('assets/frontend/js/page/index.js')}}"></script>
  <script src="{{asset('assets/frontend/js/page/forms-advanced-forms.js')}}"></script>
  <script src="{{asset('assets/frontend/js/page/components-multiple-upload.js')}}"></script>
  <script src="{{asset('assets/frontend/js/page/modules-toastr.js')}}"></script>
</body>

</html>

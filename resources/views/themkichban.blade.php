@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Thêm kịch bản cuộc gọi</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{route('addCallScript')}}" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Tên kịch bản(*)</label>
                                        <input type="text" name="call_script_name" id="call_script_name"
                                               class="form-control" required="">
                                        <span
                                            style="color: red;">@error('call_script_name'){{ $message }} @enderror</span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Nội dung kịch bản</label>
                                        <div>
                  <textarea class="summernote" name="script" id="script">
                </textarea>
                                            <span style="color: red;">@error('script'){{ $message }} @enderror</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <div class="form-group">
                                        <label></label>
                                        <div>
                                            <button class="btn btn-primary">Thêm kịch bản</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection

@push('js')

@endpush

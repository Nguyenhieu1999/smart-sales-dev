@extends('layouts.frontend.master')
@section('title','Home')
@push('css')
@endpush
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="col-md-8 col-lg-8">Cấu hình hotline</h1>
                <div class="section-header-breadcrumb">
                    <button type="button" class="text-right btn btn-primary" data-toggle="modal"
                            data-target="#Themhotline"><i class="fa fa-plus"></i>Thêm số hotline
                    </button>
                </div>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('fail'))
                <div class="alert alert-danger" id="danger-alert">
                    {{ session()->get('fail') }}
                </div>
            @endif
            <div class="section-body">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md-12">
                                <tr>
                                    <th style="display: none">ID</th>
                                    <th>Số hotline</th>
                                    <th>Tên nhà mạng</th>
                                    <th>Ảnh hợp đồng</th>
                                    <th>Ngày cập nhật</th>
                                    <th>Trạng thái</th>
                                    <th>Thao tác</th>
                                </tr>
                                @foreach($list as $item)
                                    <tr>
                                        <td style="display: none" class="id">{{$item->id}}</td>
                                        <td class="hotline_number">{{'0'.$item->hotline_number}}</td>
                                        <td class="telecom_operator">{{$item ->telecom_operator}}</td>
                                        <td><img src="{{asset('uploads/contract_image/'.$item->contract_image)}}"
                                                 width="100px;" height="150px;" alt="Image"></td>
                                        <td>{{$item->created_at}}</td>
                                        @if($item -> status == '0')
                                            <td>Chưa xác thực</td>
                                        @elseif($item -> status == '1')
                                            <td>Đã xác thực</td>
                                        @endif
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-secondary edit" data-toggle="modal"
                                                   data-idUpdate="'$item->id'" data-target="#Suahotline"><i
                                                        class="fa fa-edit"></i></a>
                                                <a href="deleteHotline/{{$item->id}}"
                                                   onclick="return confirm('Bạn có muốn xóa?')"
                                                   class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {{$list->links()}}
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--Modal sửa holine-->
    <div class="modal fade" id="Suahotline" tabindex="-1" role="dialog" aria-labelledby="SuahotlineTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Sửa số hotline</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('editHotline')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value=""/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Số hotline</label>
                            <input type="text" name="hotline_number" id="hotline_number" class="form-control" value="{{old('hotline')}}">
                            <span style="color: red;">@error('hotline_number'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Chọn nhà mạng cung cấp</label>
                            <select class="form-control selectric" name="telecom_operator">
                                <option>Viettel</option>
                                <option>Vinaphone</option>
                                <option>Mobiphone</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Ảnh hợp đồng</label>
                            <div class="fallback">
                                <input name="contract_image" type="file" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="icofont icofont-eye-alt"></i>Close
                        </button>
                        <button type="submit" id="" name="" class="btn btn-primary">Sửa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End modal sửa hotline-->

    <!--Modal thêm hotline-->
    <div class="modal fade" id="Themhotline" tabindex="-1" role="dialog" aria-labelledby="ThemhotlineTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Thêm số hotline</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('addHotline')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Số hotline</label>
                            <input type="text" name="hotline_number" class="form-control"
                                   value="{{old('hotline_number')}}">
                            <span style="color: red;">@error('hotline_number'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Chọn nhà mạng cung cấp</label>
                            <select class="form-control selectric" name="telecom_operator">
                                <option>Viettel</option>
                                <option>Vinaphone</option>
                                <option>Mobiphone</option>
                            </select>
                            <span style="color: red;">@error('telecom_operator'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Ảnh hợp đồng</label>
                            <div class="fallback">
                                <input name="contract_image" type="file" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="submit" class="btn btn-primary">Tạo số hotline</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End modal thêm hotline-->
@endsection
@push('js')
@endpush

@extends('layouts.frontend.master')
@section('title','Home')
@push('css')
@endpush
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header col-md-12 col-lg-12">
      <h1>Phiếu ghi</h1>
    </div>
    <div class="section-body">
      <div class="card">
        <div class="card-body">
          <h4>Tìm kiếm</h4>
          <div class="row">
            <div class="form-group col-md-2">
              <label>Liên hệ</label>
              <select class="form-control select2">
                <option>Thuê bao 1</option>
                <option>Thuê bao 2</option>
                <option>Thuê bao 3</option>
              </select>
            </div>
            <div class="form-group col-md-2">
              <label>Loại</label>
              <input type="text" class="form-control inputtags">
            </div>
            <div class="form-group col-md-2">
              <label>Trạng thái</label>
              <input type="text" class="form-control inputtags">
            </div>
            <div class="form-group col-md-2">
              <label>Ưu tiên</label>
              <input type="text" class="form-control inputtags">
            </div>
            <div class="form-group col-md-2">
              <label>Lý do</label>
              <input type="text" class="form-control inputtags">
            </div>
            <div class="form-group col-md-2">
              <label>Thời gian tạo</label>
              <input type="text" class="form-control datepicker">
            </div>
          </div>
          <div class="row">
            <label>Hiển thị</label>
            <div class="form-group col-2">
              <select class="form-control selectric">
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
              </select>
            </div>
            <div class="form-group col-2" style="float: right;"><a href="#" class="btn btn-primary">Tìm kiếm</a></div>
          </div>
          <br>
          <div class="row">
            <div class="card-body p-0">
              <div class="table-responsive">
                <table class="table table-striped table-md-12">
                  <tr>
                    <th>Người tạo</th>
                    <th>Lý do</th>
                    <th>Tên phiếu ghi</th>
                    <th>Giao cho</th>
                    <th>Trạng thái</th>
                    <th>Liên hệ</th>
                    <th>Loại</th>
                    <th>Ưu tiên</th>
                    <th>Thời gian tạo</th>
                    <th>Cập nhật lần cuối</th>
                    <th>ID cuộc gọi</th>
                    <th>Hạng khách hàng</th>
                    <th>Thao tác</th>
                  </tr>
                  <tr>
                    <td>ITS</td>
                    <td>Mới tạo</td>
                    <td>Gọi ra: Hieunm</td>
                    <td>ITS</td>
                    <td>Mới</td>
                    <td>Hieunm</td>
                    <td>Câu hỏi</td>
                    <td>Thấp</td>
                    <td>2021-05-16 16:20:10</td>
                    <td>2021-05-16 16:20:10</td>
                    <td></td>
                    <td>D: Có Contact chưa gọi</td>
                    <td>
                      <div class="btn-group">
                        <a href="#" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                        <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>ITS</td>
                    <td>Mới tạo</td>
                    <td>Gọi ra: Thangth</td>
                    <td>ITS</td>
                    <td>Mới</td>
                    <td>Thangth</td>
                    <td>Câu hỏi</td>
                    <td>Thấp</td>
                    <td>2021-05-16 02:57:53</td>
                    <td>2021-05-16 02:57:53</td>
                    <td></td>
                    <td>B: KH cần trao đổi thêm, hẹn trao đổi tiếp, đang chờ Setup gọi lại</td>
                    <td>
                      <div class="btn-group">
                        <a href="#" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                        <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>ITS</td>
                    <td>Mới tạo</td>
                    <td>Gọi ra: Tungth</td>
                    <td>ITS</td>
                    <td>Mới</td>
                    <td>Tungth</td>
                    <td>Câu hỏi</td>
                    <td>Thấp</td>
                    <td>2021-05-16 01:35:52</td>
                    <td>2021-05-16 01:35:52</td>
                    <td></td>
                    <td>B-: Không có nhu cầu/ Từ chối</td>
                    <td>
                      <div class="btn-group">
                        <a href="#" class="btn btn-primary"><i class="fa fa-search-plus"></i></a>
                        <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>


    </div>
</div>

@endsection
@push('js')
@endpush
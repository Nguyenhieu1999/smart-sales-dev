@extends('layouts.frontend.master')
@section('title','Home')
@push('css')
@endpush
@section('content')
    <div class="main-content">
        <div class="modal fade" id="modal_import_employee" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Import nhân viên</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('uploadAgent')}}" enctype="multipart/form-data" method="POST">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Chọn File</label>
                                <p class="text-small font-weight-600 text-muted">Yêu cầu định dạng file là xlsx, bạn có
                                    thể tải file mẫu <a href="#">tại
                                        đây</a>.</p>
                                <div class="fallback">
                                    <input name="file" type="file" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" value="{{ request()->id }}" name="member_group_id">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Tải lên</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="section-header">
                <h1>{{$group -> group_name}}</h1>
                <div class="section-header-breadcrumb" >
                    <a href="#" data-toggle="modal" data-target="#modal_import_employee" class="text-primary mr-3">Import
                        nhân viên</a>
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#Themnhanvien"><i
                            class="fa fa-plus"></i>Thêm nhân viên</a>
                </div>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('fail'))
                <div class="alert alert-danger" id="danger-alert">
                    {{ session()->get('fail') }}
                </div>
            @endif
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped table-md-12">
                                        <tr>
                                            <th style="display: none">ID</th>
                                            <th>Tên nhân viên</th>
                                            <th>Điện thoại</th>
                                            <th>Địa chỉ</th>
                                            <th>Email</th>
                                            <th style="display: none">Password</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        @foreach($listAgent as $item)
                                            <tr>
                                                <td style="display: none" class="id">{{$item->id}}</td>
                                                <td class="member_name">{{$item->member_name}}</td>
                                                <td class="member_phone">{{'0'.$item->member_phone}}</td>
                                                <td class="member_address">{{$item->member_address}}</td>
                                                <td class="member_email">{{$item->member_email}}</td>
                                                <td style="display: none" class="member_password">{{$item->member_password}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn btn-secondary edit" data-toggle="modal"
                                                           data-idUpdate="'$value->id'" data-target="#Suanhanvien"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a href="/agent/deleteAgent/{{$item->id}}"
                                                           onclick="return confirm('Bạn có muốn xóa?')"
                                                           class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                {{$listAgent->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--Modal thêm nhân viên-->
    <div class="modal fade" id="Themnhanvien" tabindex="-1" role="dialog" aria-labelledby="ThemnhanvienTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Thêm nhân viên</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('addAgent')}}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Họ và tên</label>
                            <input type="text" name="member_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <input type="text" name="member_address" class="form-control">

                        </div>
                        <div class="form-group">
                            <label>Số điện thoại</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-phone"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control phone-number" name="member_phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email đăng nhập</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" placeholder="Email" name="member_email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-lock"></i>
                                    </div>
                                </div>
                                <input type="password" class="form-control pwstrength" data-indicator="pwindicator"
                                       placeholder="Password" name="member_password">
                            </div>
                            <div id="pwindicator" class="pwindicator">
                                <div class="bar"></div>
                                <div class="label"></div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" value="{{ request()->id }}" name="member_group_id">

                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="icofont icofont-eye-alt"></i>Close
                        </button>
                        <button type="submit" class="btn btn-primary">Thêm nhân viên</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End modal thêm nhân viên-->


    <!--Modal sửa nhân viên-->
    <div class="modal fade" id="Suanhanvien" tabindex="-1" role="dialog" aria-labelledby="SuanhanvienTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Cập nhật</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('updateAgent')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value=""/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Họ và tên</label>
                            <input type="text" name="member_name" id="member_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <input type="text" name="member_address" id="member_address" class="form-control">

                        </div>
                        <div class="form-group">
                            <label>Số điện thoại</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-phone"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control phone-number" name="member_phone"
                                       id="member_phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email đăng nhập</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" placeholder="Email" name="member_email"
                                       id="member_email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-lock"></i>
                                    </div>
                                </div>
                                <input type="password" class="form-control pwstrength" data-indicator="pwindicator"
                                       placeholder="Password" name="member_password" id="member_password">
                            </div>
                            <div id="pwindicator" class="pwindicator">
                                <div class="bar"></div>
                                <div class="label"></div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" value="{{ request()->id }}" name="member_group_id">

                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="icofont icofont-eye-alt"></i>Close
                        </button>
                        <button type="submit" class="btn btn-primary">Sửa nhân viên</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End modal sửa nhân viên-->
@endsection
@push('js')
@endpush

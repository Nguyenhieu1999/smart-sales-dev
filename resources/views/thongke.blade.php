@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1 class="col-8">Số liệu thống kê cho chiến dịch số 1</h1>
      <div class="fc-toolbar-chunk text-right col-4">
        <div class="input-group">
          <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250" style="width: 250px;">
          <div class="input-group-append">
            <button class="btn btn-outline-primary waves-effect btn_filter" type="button">Lọc !</button>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body statistics-body">
        <div class="row">
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-info mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 9px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-graph-up" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M0 0h1v15h15v1H0V0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">8</h4>
                <p class="card-text font-small-3 mb-0">Tổng thành viên</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-warning mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng khách Big Data</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-danger mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-box" viewBox="0 0 16 16">
                    <path d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5 8 5.961 14.154 3.5 8.186 1.113zM15 4.239l-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923l6.5 2.6zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464L7.443.184z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">4</h4>
                <p class="card-text font-small-3 mb-0">Tổng khách nội bộ</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-success  mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-outbound" viewBox="0 0 16 16">
                    <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng cuộc gọi ra</p>
              </div>
            </div>
          </div>


          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-info mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-inbound" viewBox="0 0 16 16">
                    <path d="M15.854.146a.5.5 0 0 1 0 .708L11.707 5H14.5a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793L15.146.146a.5.5 0 0 1 .708 0zm-12.2 1.182a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng cuộc gọi vào</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-warning mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-x" viewBox="0 0 16 16">
                    <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                    <path fill-rule="evenodd" d="M11.146 1.646a.5.5 0 0 1 .708 0L13 2.793l1.146-1.147a.5.5 0 0 1 .708.708L13.707 3.5l1.147 1.146a.5.5 0 0 1-.708.708L13 4.207l-1.146 1.147a.5.5 0 0 1-.708-.708L12.293 3.5l-1.147-1.146a.5.5 0 0 1 0-.708z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng không liên lạc dược</p>
              </div>
            </div>
          </div>



          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-danger mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
                    <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng cuộc gọi gặp</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-success mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-telephone-minus" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z" />
                    <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng cuộc gọi nhỡ</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-info mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-file-earmark-text" viewBox="0 0 16 16">
                    <path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z" />
                    <path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng phiếu ghi</p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-sm-3 col-3 mb-2 mt-2 mb-xl-0">
            <div class="media">
              <div class="avatar bg-warning mr-2">
                <div class="avatar-content">
                  <svg style="display: flex; margin: 10px;" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cloud-lightning" viewBox="0 0 16 16">
                    <path d="M13.405 4.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 10H13a3 3 0 0 0 .405-5.973zM8.5 1a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 1zM7.053 11.276A.5.5 0 0 1 7.5 11h1a.5.5 0 0 1 .474.658l-.28.842H9.5a.5.5 0 0 1 .39.812l-2 2.5a.5.5 0 0 1-.875-.433L7.36 14H6.5a.5.5 0 0 1-.447-.724l1-2z" />
                  </svg>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">0</h4>
                <p class="card-text font-small-3 mb-0">Tổng chiến dịch gọi</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-12 col-sm-12 col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>Summary</h4>
            <div class="card-header-action">
              <a href="#summary-chart" data-tab="summary-tab" class="btn active">Chart</a>
              <a href="#summary-text" data-tab="summary-tab" class="btn">Text</a>
            </div>
          </div>
          <div class="card-body">
            <div class="summary">
              <div class="summary-info" data-tab-group="summary-tab" id="summary-text">
                <h4>$1,858</h4>
                <div class="text-muted">Sold 4 items on 2 customers</div>
                <div class="d-block mt-2">
                  <a href="#">View All</a>
                </div>
              </div>
              <div class="summary-chart active" data-tab-group="summary-tab" id="summary-chart">
                <canvas id="myChart" height="180"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>Visitors</h4>
          </div>
          <div class="card-body">
            <div id="visitorMap" data-height="190"></div>
          </div>
          <div class="card-footer card-footer-grey pt-0">
            <div class="statistic-details mt-4 align-items-center justify-content-center">
              <div class="statistic-details-item col-sm-4 col-12">
                <div class="detail-chart">
                  <div class="sparkline-line-chart"></div>
                </div>
                <div class="detail-value">12,329</div>
                <div class="detail-name">Visits</div>
              </div>
              <div class="statistic-details-item col-sm-4 col-12">
                <div class="detail-chart">
                  <div class="sparkline-line-chart"></div>
                </div>
                <div class="detail-value">28%</div>
                <div class="detail-name">Referral</div>
              </div>
              <div class="statistic-details-item col-sm-4 col-12">
                <div class="detail-chart">
                  <div class="sparkline-line-chart"></div>
                </div>
                <div class="detail-value">72%</div>
                <div class="detail-name">Organic</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
</div>
@endsection

@push('js')

@endpush
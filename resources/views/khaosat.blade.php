@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header col-12">
                <h1 class="col-6">Khảo sát</h1>
                <div class="text-right col-6">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Themkhaosat"><i
                            class="fa fa-plus"></i>Thêm khảo sát
                    </button>
                </div>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('fail'))
                <div class="alert alert-danger" id="danger-alert">
                    {{ session()->get('fail') }}
                </div>
            @endif
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped table-md-12">
                                        <tr>
                                            <th style="display: none">ID</th>
                                            <th>Tên khảo sát</th>
                                            <th>Thể loại</th>
                                            <th>Ngày cập nhật</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        @foreach($list as $items)
                                            <tr>
                                                <td style="display: none">{{$items->id}}</td>
                                                <td>{{$items->survey_name}}</td>
                                                <td>
                                                    @if($items->survey_type == 1)
                                                        Một đáp án
                                                    @elseif($items->survey_type == 2)
                                                        Nhiều đáp án
                                                    @elseif($items->survey_type == 3)
                                                        Chấm điểm
                                                    @endif
                                                </td>
                                                <td>{{$items->created_at}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn btn-secondary edit"
                                                           onclick="editKhaoSat({{$items->id}});"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a href="deleteSurvey/{{$items->id}}"
                                                           onclick="return confirm('Bạn có muốn xóa?')"
                                                           class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                {{$list->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <!--Thêm khảo sát -->
    <div class="modal fade" id="Themkhaosat" tabindex="-1" role="dialog" aria-labelledby="ThemkhaosatTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Thêm khảo sát</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('addSurvey')}}" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tên khảo sát</label>
                            <input type="text" class="form-control" name="survey_name" id="survey_name">
                        </div>
                        <div class="form-group">
                            <label>Loại khảo sát</label>
                            <select class="form-control selectric" onchange="displayDivDemo('hideValuesOnSelect', this)"
                                    name="survey_type" id="survey_type">
                                <option value="1">Chọn một đáp án</option>
                                <option value="2">Chọn nhiều đáp án</option>
                                <option value="3">Chấm điểm</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Câu hỏi</label>
                            <input type="text" class="form-control"
                                   placeholder="Bạn đánh giá thế nào về dịch vụ của chúng tôi?" name="survey_question"
                                   id="survey_question">
                        </div>

                        <div id="surveyOneAnswer">
                            <div class="form-group">
                                <label>Nhập câu trả lời: </label>
                                <input class="form-control" type='textbox' name="survey_answer">
                            </div>
                        </div>

                        <div id="surveyMultiAnswer" style="display: none;">
                            <div class="form-group" id='TextBoxesGroup'>
                                <label>Nhập câu trả lời: </label>
                                <input class="form-control" type='textbox' name="survey_multi_answer[]">
                            </div>

                            <div class="form-group">
                                <input type='button' class="btn btn-primary" value='Thêm câu trả lời' id='addButton'>
                                <input type='button' class="btn btn-danger" value='Xóa câu trả lời' id='removeButton'>
                            </div>
                        </div>

                        <div id="surveyScore" style="display: none;">
                            <div class="form-group">
                                <label for="number">Điểm từ:</label>
                                <input type="number" min="0" max="5" class="form-control" placeholder="1"
                                       name="survey_score_min" id="survey_score_min">
                                <label for="number">đến:</label>
                                <input type="number" min="0" max="5" class="form-control" placeholder="5"
                                       name="survey_score_max" id="survey_score_max">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary">Tạo khảo sát</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End thêm khảo sát -->

    <!--Sửa khảo sát -->
    <form method="POST" action="{{route('updateSurveyInfo')}}" id="updateSurveyInfoForm">
        {{csrf_field()}}
        <input type="hidden" class="form-control" id="updateSurveyInfoSurveyID" name="updateSurveyInfoSurveyID"/>
    </form>

    <div class="modal fade" id="Suakhaosat" tabindex="-1" role="dialog" aria-labelledby="SuakhaosatTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Sửa khảo sát</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('updateSurvey')}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" class="col-sm-9 form-control" id="edit_id" name="edit_id" value=""/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tên khảo sát</label>
                            <input type="text" class="form-control" name="edit_survey_name" id="edit_survey_name">
                        </div>
                        <div class="form-group">
                            <label>Loại khảo sát</label>
                            <select class="form-control selectric" name="edit_survey_type" id="edit_survey_type">
                                <option value="1">Chọn một đáp án</option>
                                <option value="2">Chọn nhiều đáp án</option>
                                <option value="3">Chấm điểm</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Câu hỏi</label>
                            <input type="text" class="form-control"
                                   placeholder="Bạn đánh giá thế nào về dịch vụ của chúng tôi?"
                                   name="edit_survey_question"
                                   id="edit_survey_question">
                        </div>

                        <div id="e_surveyOneAnswer">
                            <div class="form-group">
                                <label>Nhập câu trả lời: </label>
                                <input class="form-control" type='textbox' name="edit_survey_answer"
                                       id="edit_survey_answer">
                            </div>
                        </div>

                        <div id="e_surveyMultiAnswer" style="display: none;">

                            <div class="form-group">
                                <input type='button' class="btn btn-primary" value='Thêm câu trả lời' id='e_addButton'>
                                <input type='button' class="btn btn-danger" value='Xóa câu trả lời' id='e_removeButton'>
                            </div>
                        </div>

                        <div id="e_surveyScore" style="display: none;">
                            <div class="form-group">
                                <label for="number">Điểm từ:</label>
                                <input type="number" min="0" max="5" class="form-control" placeholder="1"
                                       name="edit_survey_score_min" id="edit_survey_score_min">
                                <label for="number">đến:</label>
                                <input type="number" min="0" max="5" class="form-control" placeholder="5"
                                       name="edit_survey_score_max" id="edit_survey_score_max">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="submit" id="" name="" class="btn btn-primary">Sửa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End sửa khảo sát -->
@endsection

@push('js')

@endpush

@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header col-12">
                <h1 class="col-6">Kịch bản cuộc gọi</h1>
                <div class="text-right col-6"><a href="{{url('/addScript')}}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Thêm kịch bản</a></div>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('fail'))
                <div class="alert alert-danger" id="danger-alert">
                    {{ session()->get('fail') }}
                </div>
            @endif
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped table-md-12">
                                        <tr>
                                            <th style="display: none">ID</th>
                                            <th>Tên kịch bản</th>
                                            <th>Ngày cập nhật</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        @foreach($call_script as $items)
                                            <tr>
                                                <td style="display: none" class="id">{{$items->id}}</td>
                                                <td class="call_script_name">{{ $items->call_script_name }}</td>
                                                <td class="created_at">{{$items->created_at}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="{{route('editcallscript',[$items->id,Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}"
                                                           class="btn btn-sm btn-secondary"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a href="deleteScript/{{$items->id}}" onclick="return confirm('Bạn có muốn xóa?')" class="btn btn-sm btn-danger"><i
                                                                class="fa fa-trash"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                {{$call_script->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('js')

@endpush

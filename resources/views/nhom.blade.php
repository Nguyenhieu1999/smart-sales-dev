@extends('layouts.frontend.master')
@section('title','Home')
@push('css')
@endpush
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1 class="col-md-8 col-lg-8">Nhóm nhân viên</h1>
                <div class="section-header-breadcrumb">
                    <button type="button" class="text-right btn btn-primary" data-toggle="modal"
                            data-target="#Themnhom"><i class="fa fa-plus"></i>Thêm nhóm
                    </button>
                </div>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('fail'))
                <div class="alert alert-danger" id="danger-alert">
                    {{ session()->get('fail') }}
                </div>
            @endif
            <div class="section-body">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md-12">
                                <tr>
                                    <th style="display: none">ID</th>
                                    <th>Tên nhóm</th>
                                    <th>Số thành viên</th>
                                    <th>Ngày cập nhật</th>
                                    <th>Tag</th>
                                    <th>Thao tác</th>
                                </tr>
                                @foreach($list as $item)
                                    <tr>
                                        <td style="display: none" class="id">{{$item->id}}</td>
                                        <td class="group_name"><a
                                                href="{{route('agent',[$item -> id,Session::get('LoggedUser') -> id,Session::get('LoggedUser') -> slug])}}">{{$item->group_name}}</a></td>
                                        <td class="group_id">{{$item -> listOfAgent()->count()}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td class="tag">
                                            @foreach(explode(',', $item->tag) as $info)
                                                <div class="badge">{{$info}}</div>
                                            @endforeach</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{url('/agent',[$item -> id])}}" class="btn btn-primary"><i
                                                        class="fa fa-info"></i></a>
                                                <a class="btn btn-secondary edit" data-toggle="modal"
                                                   data-idUpdate="'$value->id'" data-target="#Suanhom"><i
                                                        class="fa fa-edit"></i></a>
                                                <a href="delete/{{$item->id}}"
                                                   onclick="return confirm('Bạn có muốn xóa?')"
                                                   class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        {{--                        {{$list->links()}}--}}
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!--Modal sửa nhóm-->
    <div class="modal fade" id="Suanhom" tabindex="-1" role="dialog" aria-labelledby="SuanhomTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Sửa nhóm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('update')}}">
                    {{ csrf_field() }}
                    <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value=""/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tên nhóm</label>
                            <input type="text" name="group_name" id="group_name" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>Tag nhóm</label>
                            <input type="text" name="tag" id="tag" class="form-control inputtags" value="">
                        </div>

                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="icofont icofont-eye-alt"></i>Close
                        </button>
                        <button type="submit" id="" name="" class="btn btn-primary">Sửa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End modal sửa nhóm-->

    <!--Modal thêm nhóm-->
    <div class="modal fade" id="Themnhom" tabindex="-1" role="dialog" aria-labelledby="ThemnhomTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Thêm nhóm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('addGroup')}}" class="needs-validation" novalidate="">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tên nhóm</label>
                            <input type="text" name="group_name" class="form-control" value="{{old('group_name')}}">
                            <span style="color: red;">@error('group_name'){{ $message }} @enderror</span>
                        </div>
                        <div class="form-group">
                            <label>Tag nhóm</label>
                            <input type="text" name="tag" class="form-control inputtags" value="{{old('tag')}}">
                            <span style="color: red;">@error('tag'){{ $message }} @enderror</span>
                        </div>

                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="submit" class="btn btn-primary">Tạo nhóm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End modal thêm nhóm-->
@endsection

@push('js')

@endpush

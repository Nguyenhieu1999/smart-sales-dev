@extends('layouts.frontend.master')

@section('title', 'Home')

    @push('css')

    @endpush

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header col-md-12 col-lg-12">
                <h1 class="col-md-6">Cuộc gọi #92313AS</h1>
                <div class="section-header-breadcrumb">
                    <a class="btn btn-primary" href="#"><i class="fa fa-forward"></i> Cuộc gọi tiếp theo</a>
                </div>
            </div>
            <div class="section-body">
                <div class="row">

                    <div class="col-12 col-md-6 col-lg-6">

                        <div class="card">
                            <div class="card-header">
                                <h4>Khảo sát khách hàng</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Chúng tôi có hiểu biết và hữu ích không?</label>
                                    <select class="form-control selectric">
                                        <option>Có</option>
                                        <option>Không</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Chúng tôi có phục vụ nhanh chóng không?</label>
                                    <select class="form-control selectric">
                                        <option>Có</option>
                                        <option>Không</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Bạn còn điều gì muốn phản ánh?</label>
                                    <textarea class="form-control" required=""
                                        placeholder="Vui lòng viết vào đây"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card">

                            <div class="card-header">
                                <h4>Kịch bản mẫu</h4>
                            </div>
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>Khách hàng</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="d-flex flex-column">
                                        <span class="font-weight-bold">Nguyễn Văn A</span>
                                        <span>098xxxx343</span>
                                        <span>70/165, Xuân Thủy, Cầu Giấy, Hà Nội</span>
                                    </div>
                                </div>

                                <div class="form-group mb-0">
                                    <p><i class="fa fa-building"></i> Cầu Giấy, Hà Nội</p>
                                    <p><i class="fa fa-envelope"></i> Admin@gmail.com</p>
                                    <p><i class="fa fa-graduation-cap"></i> Kỹ sư CNTT</p>
                                    <p><i class="fa fa-users"></i> Giải pháp phần mềm</p>
                                    <p><i class="fa fa-link"></i> interits.com</p>
                                    <p><i class="fa fa-map-marker"></i> Hà Nội</p>
                                    <p><i class="fa fa-globe"></i> Việt Nam</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Tạo phiếu ghi</label>

                                        <select class="form-control selectric">
                                            <option>D: Có contact chưa gọi</option>
                                            <option>B: KH cần trao đổi thêm, hẹn trao đổi tiếp, đang chờ Setup gọi lại
                                            </option>
                                            <option>B-: Không có nhu cầu/ Từ chối</option>
                                            <option>C+: Sai số không nhấc máy</option>
                                            <option>B+: Đã gặp/Đã nhận bảng giá/Đã demo</option>
                                            <option>A-: KH đã lên đơn hàng/Đã lên hợp đồng nhưng chưa thanh toán</option>
                                            <option>A: Đã trả về HĐ bản cứng nhưng chưa thanh toán</option>
                                            <option>A+: Đã thanh toán</option>
                                            <option>Đang hỗ trợ sau bán hàng</option>
                                            <option>Upsell dịch vụ mới</option>

                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Ghi chú cuộc gọi" data-height="150"
                                            style="height: 150px;"></textarea>
                                    </div>

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-round btn-lg btn-primary">
                                            Ghi chú
                                        </button>
                                    </div>

                                    <div class="activities">
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="fas fa-comment-alt"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <div class="mb-2">
                                                    <span class="text-job text-primary">2 min ago</span>
                                                    <span class="bullet"></span>
                                                    <a class="text-job" href="#">View</a>
                                                    <div class="float-right dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-eye"></i> View</a>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-list"></i> Detail</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#"
                                                                class="dropdown-item has-icon text-danger trigger--fire-modal-1"
                                                                data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?"
                                                                data-confirm-text-yes="Yes, IDC"><i
                                                                    class="fas fa-trash-alt"></i> Archive</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Have commented on the task of "<a href="#">Responsive design</a>".</p>
                                            </div>
                                        </div>
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="fas fa-arrows-alt"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <div class="mb-2">
                                                    <span class="text-job">1 hour ago</span>
                                                    <span class="bullet"></span>
                                                    <a class="text-job" href="#">View</a>
                                                    <div class="float-right dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-eye"></i> View</a>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-list"></i> Detail</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#"
                                                                class="dropdown-item has-icon text-danger trigger--fire-modal-2"
                                                                data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?"
                                                                data-confirm-text-yes="Yes, IDC"><i
                                                                    class="fas fa-trash-alt"></i> Archive</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Moved the task "<a href="#">Fix some features that are bugs in the master
                                                        module</a>" from Progress to Finish.</p>
                                            </div>
                                        </div>
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="fas fa-unlock"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <div class="mb-2">
                                                    <span class="text-job">4 hour ago</span>
                                                    <span class="bullet"></span>
                                                    <a class="text-job" href="#">View</a>
                                                    <div class="float-right dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-eye"></i> View</a>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-list"></i> Detail</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#"
                                                                class="dropdown-item has-icon text-danger trigger--fire-modal-3"
                                                                data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?"
                                                                data-confirm-text-yes="Yes, IDC"><i
                                                                    class="fas fa-trash-alt"></i> Archive</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Login to the system with ujang@maman.com email and location in Bogor.</p>
                                            </div>
                                        </div>
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="fas fa-sign-out-alt"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <div class="mb-2">
                                                    <span class="text-job">12 hour ago</span>
                                                    <span class="bullet"></span>
                                                    <a class="text-job" href="#">View</a>
                                                    <div class="float-right dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-eye"></i> View</a>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-list"></i> Detail</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#"
                                                                class="dropdown-item has-icon text-danger trigger--fire-modal-4"
                                                                data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?"
                                                                data-confirm-text-yes="Yes, IDC"><i
                                                                    class="fas fa-trash-alt"></i> Archive</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Log out of the system after 6 hours using the system.</p>
                                            </div>
                                        </div>
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <div class="mb-2">
                                                    <span class="text-job">Yesterday</span>
                                                    <span class="bullet"></span>
                                                    <a class="text-job" href="#">View</a>
                                                    <div class="float-right dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-eye"></i> View</a>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-list"></i> Detail</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#"
                                                                class="dropdown-item has-icon text-danger trigger--fire-modal-5"
                                                                data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?"
                                                                data-confirm-text-yes="Yes, IDC"><i
                                                                    class="fas fa-trash-alt"></i> Archive</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Removing task "Delete all unwanted selectors in CSS files".</p>
                                            </div>
                                        </div>
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white shadow-primary">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <div class="mb-2">
                                                    <span class="text-job">Yesterday</span>
                                                    <span class="bullet"></span>
                                                    <a class="text-job" href="#">View</a>
                                                    <div class="float-right dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-eye"></i> View</a>
                                                            <a href="#" class="dropdown-item has-icon"><i
                                                                    class="fas fa-list"></i> Detail</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="#"
                                                                class="dropdown-item has-icon text-danger trigger--fire-modal-6"
                                                                data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?"
                                                                data-confirm-text-yes="Yes, IDC"><i
                                                                    class="fas fa-trash-alt"></i> Archive</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p>Assign the task of "<a href="#">Redesigning website header and make it
                                                        responsive AF</a>" to <a href="#">Syahdan Ubaidilah</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>

                </div>

            </div>
    </div>

@endsection

@push('js')

@endpush

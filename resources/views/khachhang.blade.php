@extends('layouts.frontend.master')
@section('title','Home')
@push('css')
@endpush
@section('content')
    <div class="main-content">
        <!--Modal import -->
        <div class="modal fade" id="modal_import_customers" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Import khách hàng</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('uploadCustomer')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Chọn File</label>
                                <p class="text-small font-weight-600 text-muted">Yêu cầu định dạng file là xlsx, bạn có
                                    thể tải file mẫu <a href="#">tại đây</a>.</p>
                                <div class="fallback">
                                    <input name="file" type="file" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Gắn tags</label>
                                <input type="text" class="form-control inputtags" name="tag" id="tag">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Tải lên</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End modal import -->

        <!--Modal add -->
        <div class="modal fade" id="modal_add_customers" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Thêm khách hàng</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('addCustomer')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Họ và tên</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fas fa-map-marker"></i>
                                            </div>
                                        </div>
                                        <input type="text" class="form-control" name="address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Điện thoại</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fas fa-phone"></i>
                                            </div>
                                        </div>
                                        <input type="text" class="form-control phone-number" name="phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Tags</label>
                                    <input type="text" class="form-control inputtags" name="tag">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thêm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--End modal add -->
        <section class="section">
            <div class="section-header col-md-12 col-lg-12">
                <h1 class="col-md-9 col-lg-9">Khách hàng</h1>
                <div style="margin-left: auto"><a class="btn btn-primary" href="#" data-toggle="modal"
                                                  data-target="#modal_import_customers"><i
                            class="fa fa-plus"></i> Import khách hàng</a></div>
                <div style="margin: auto"><a class="btn btn-primary" href="#" data-toggle="modal"
                                             data-target="#modal_add_customers"><i
                            class="fa fa-user-plus"></i> Thêm khách hàng</a></div>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success" id="success-alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('fail'))
                <div class="alert alert-danger" id="danger-alert">
                    {{ session()->get('fail') }}
                </div>
            @endif
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped table-md-12">
                                        <tr>
                                            <th style="display: none">ID</th>
                                            <th>Tên khách hàng</th>
                                            <th>Địa chỉ</th>
                                            <th>Điện thoại</th>
                                            <th>Tag</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        <tr>
                                            @foreach($customer as $items)
                                                <td style="display: none" class="id">{{$items->id}}</td>
                                                <td class="name">{{$items->name}}</td>
                                                <td class="address">{{$items->address}}</td>
                                                <td class="phone">{{'0'.$items->phone}}</td>
                                                <td class="tags">

                                                    @foreach(explode(',', $items->tag) as $info)

                                                        <div class="badge">{{$info}}</div>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn btn-secondary edit" data-toggle="modal"
                                                           data-idUpdate="'$value->id'" data-target="#Suakhachhang"><i
                                                                class="fa fa-edit"></i></a>
                                                        <a href="deleteCustomer/{{$items->id}}"
                                                           onclick="return confirm('Bạn có muốn xóa?')"
                                                           class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                    </div>
                                                </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                {{$customer->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!--Modal sửa thông tin khách hàng-->
    <div class="modal fade" id="Suakhachhang" tabindex="-1" role="dialog" aria-labelledby="SuakhachhangTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Sửa khách hàng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('updateCustomer')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="text" hidden class="col-sm-9 form-control" id="id" name="id" value=""/>
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Họ và tên</label>
                                <input type="text" class="form-control" name="name" id="name">
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-map-marker"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" name="address" id="address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Điện thoại</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-phone"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control phone-number" name="phone" id="phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <input type="text" class="form-control inputtags" name="tags" id="editTags">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary">Sửa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End Modal sửa thông tin khách hàng-->

@endsection

@push('js')

@endpush

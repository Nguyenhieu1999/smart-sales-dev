@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Thêm chiến dịch</h1>
    </div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <div class="form-group">
                    <label>Tên chiến dịch(*)</label>
                    <input type="text" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Trạng thái</label>
                    <select class="form-control selectric">
                      <option>New</option>
                      <option>Active</option>
                      <option>Deactive</option>
                    </select>
                  </div>

                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                      Bạn có muốn lên lịch chiến dịch?
                    </label>
                  </div>

                  <div class="form-group">
                    <label>Chọn liên lạc theo tag</label>
                    <select class="form-control select2" multiple="">
                      <option>Tag 1</option>
                      <option>Tag 2</option>
                      <option>Tag 3</option>
                      <option>Tag 4</option>
                      <option>Tag 5</option>
                      <option>Tag 6</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Số lần gọi lại</label>
                    <input type="number" class="form-control" required="">
                  </div>

                  <div class="form-group">
                    <label>Giờ gọi lại</label>
                    <input type="number" class="form-control" required="">
                  </div>

                  <div class="form-group">
                    <label>Thời gian chờ cuộc gọi</label>
                    <input type="number" class="form-control" required="">
                  </div>

                  <div class="form-group">
                    <div class="control-label ">Tự động gọi thuê bao tiếp theo sau khi tắt máy (Bật/Tắt)</div>
                    <label class="custom-switchhh">
                      <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                      <span class="custom-switch-indicator"></span>
                    </label>
                  </div>

                  <div class="form-group" id="myDIV" style="display:none;">
                    <label>Thời gian chờ cuộc gọi tiếp theo (Giây)</label>
                    <input type="number" class="form-control" required="">
                  </div>

                  <div class="form-group">
                    <div class="control-label ">Ghi âm cuộc gọi (Bật/Tắt)</div>
                    <label class="custom-switchh">
                      <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                      <span class="custom-switch-indicator"></span>
                    </label>
                  </div>

                  <div class="form-group">
                    <label>ID Người gọi</label>
                    <select class="form-control selectric">
                      <option>Sử dụng ID người gọi đi được chỉ định cho các đại lý</option>
                      <option>Sử dụng ID người gọi đi được chỉ định cho các cá nhân</option>
                      <option>Sử dụng ID người gọi đi được chỉ định cho các doanh nghiệp</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Chỉ định kịch bản cuộc gọi</label>
                    <select class="form-control selectric">
                      <option>Kịch bản cho team Sale</option>
                      <option>Kịch bản cho team Marketing</option>
                      <option>Kịch bản cho người dùng</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Chỉ định khảo sát</label>
                    <select class="form-control selectric">
                      <option>Khảo sát cho khách hàng VIP</option>
                      <option>Khảo sát cho khách hàng mặc định</option>
                      <option>Khảo sát khác</option>
                    </select>
                  </div>

                  <div class="section-body">
                    <h4>Chỉ định trực tiếp cá nhân hoặc nhóm</h4>
                  </div>

                  <div class="form-group">
                    <label>Chỉ định nhóm</label>
                    <select class="form-control select2" multiple="">
                      <option>Nhóm Sales</option>
                      <option>Nhóm Marketing</option>
                      <option>Nhóm bán hàng</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Chỉ định cá nhân</label>
                    <input type="text" class="form-control" required="">
                  </div>
                  <div class="card-footer text-right">
                    <div class="text-right"><button class="btn btn-primary">
                        Thêm chiến dịch</button></div>
                  </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  Tính năng đang được phát triển, vui lòng quay lại sau!!!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

@endsection

@push('js')

@endpush
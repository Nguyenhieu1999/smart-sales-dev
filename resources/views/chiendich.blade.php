@extends('layouts.frontend.master')
@section('title', 'Home')
    @push('css')
    @endpush
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header col-md-12 col-lg-12">
                <h1 class="col-md-6 col-lg-6">Chiến dịch</h1>
                <div class="text-right col-md-6 col lg-6"><button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#Themchiendich"><i class="fa fa-plus"></i>Thêm chiến dịch</button></div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped table-md-12">
                                        <tr>
                                            <th>Tên chiến dịch</th>
                                            <th>Tổng số thuê bao</th>
                                            <th>Số thuê bao đã gọi</th>
                                            <th>Số thuê bao còn lại</th>
                                            <th>Trạng thái</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        <tr>
                                            <td>Chiến dịch số 1</td>
                                            <td>100</td>
                                            <td>85</td>
                                            <td>15</td>
                                            <td>
                                                <div class="badge badge-info">Đang chạy</div>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ url('/chitietchiendich') }}" class="btn btn-primary"><i
                                                            class="fa fa-search-plus"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chiến dịch số 2</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>0</td>
                                            <td>
                                                <div class="badge badge-success">Đã hoàn thành</div>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ url('/chitietchiendich') }}" class="btn btn-primary"><i
                                                            class="fa fa-search-plus"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chiến dịch số 3</td>
                                            <td>50</td>
                                            <td>15</td>
                                            <td>35</td>
                                            <td>
                                                <div class="badge badge-warning">Đã dừng</div>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ url('/chitietchiendich') }}" class="btn btn-primary"><i
                                                            class="fa fa-search-plus"></i></a>
                                                    <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                    <ul class="pagination mb-0">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" tabindex="-1"><i
                                                    class="fas fa-chevron-left"></i></a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1 <span
                                                    class="sr-only">(current)</span></a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">2</a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


    <!--Modal thêm chiến dịch-->
    <div class="modal fade" id="Themchiendich" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="overflow-y: initial !important">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm chiến dịch</h5> <button type="button" class="close"
                        data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body" style="overflow-y: auto;">
                    <div id="wizard">

                        <h3>Step 1 Title</h3>
                        <!--Cài đặt -->
                        <section>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Thiết lập chung</h4>
                                </div>

                                <div class="card-body">

                                    <div class="form-group">
                                        <label>Chế độ</label>
                                        <select class="form-control selectric">

                                            <option>Theo lịch 1 lần</option>
                                            <option>Theo lịch hàng ngày</option>
                                            <option>Theo thời gian thực</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Khung giờ tương tác</label>
                                        <div class="row">
                                            <div class="col-6 form-group">
                                                <label>Từ:</label>
                                                <input type="text" class="form-control timepicker">
                                            </div>
                                            <div class="col-6 form-group">
                                                <label>Đến:</label>
                                                <input type="text" class="form-control timepicker">
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <label>Thời gian thực hiện</label>

                                        <div class="row">
                                            <div class="col-6 form-group">
                                                <label>Từ:</label>
                                                <input type="text" class="form-control datetimepicker">
                                            </div>
                                            <div class="col-6 form-group">
                                                <label>Đến:</label>
                                                <input type="text" class="form-control datetimepicker">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <div class="control-label ">Chọn ngày trong tuần</div>
                                            <label class="custom-switchhh">
                                                <input type="checkbox" name="custom-switch-checkbox"
                                                    class="custom-switch-input" value="1">
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                        </div>
                                        <div class="form-group col-6">
                                            <div class="control-label ">Chọn ngày trong tháng</div>
                                            <label class="custom-switch2">
                                                <input type="checkbox" name="custom-switch-checkbox"
                                                    class="custom-switch-input" value="2">
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="hidden" style="display: none;">
                                        <label>Chọn ngày trong tuần</label>
                                        <select class="form-control selectric" multiple="">
                                            <option>Thứ 2</option>
                                            <option>Thứ 3</option>
                                            <option>Thứ 4</option>
                                            <option>Thứ 5</option>
                                            <option>Thứ 6</option>
                                            <option>Thứ 7</option>
                                            <option>Chủ nhật</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="hidden1" style="display: none;">
                                        <label>Chọn ngày trong tháng</label>
                                        <input type="text" class="form-control datepicker">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--End Cài đặt -->
                        <h3>Step 2 Title</h3>
                        <!-- Khách hàng -->
                        <section>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Khách hàng</h4>

                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Chọn tag khách hàng</label>
                                        <select class="form-control selectric" multiple="">
                                            <option>Tag khách hàng VIP</option>
                                            <option>Tag khách hàng tiềm năng</option>
                                            <option>Tag khách hàng mới</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Điều kiện khách hàng</label>
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <select class="form-control selectric">
                                                    <option>Không gọi lại cho khách hàng đã xử lý trong chiến dịch</option>
                                                    <option>Luôn thực hiện gọi cho khách hàng gặp điều kiện</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Blacklist
                                            <p class="section-lead text-muted">(Chiến dịch sẽ không gọi điện đến các số điện
                                                thoại
                                                này, vui
                                                lòng nhập các số cần chặn và cách nhau bởi dấu phẩy)</p>
                                        </label>
                                        <textarea class="form-control" style="width:540px; height: 150px;"></textarea>


                                    </div>

                                    <div class="form-group">
                                        <label>Dùng file Blacklist của bạn</label>
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="file-input">
                                                    <i class="attach-doc fa fa-upload fa-2x" aria-hidden="true"
                                                        style="cursor: pointer;"></i>Tải file lên
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <input style="display:none;" id="file-input" type="file" />
                                                <i class=" fa fa-file-excel" aria-hidden="true"></i><a href="#">Tải file
                                                    mẫu</a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label>Áp dụng Blacklist cho</label>
                                        <select class="form-control selectric">
                                            <option>Chỉ chiến dịch này</option>
                                            <option>Cho tất cả chiến dịch</option>
                                        </select>
                                    </div>
                                </div>
                            </div>




                        </section>
                        <!--End Khách hàng -->
                        <h3>Step 3 Title</h3>
                        <!--Kịch bản -->
                        <section>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Kịch bản và khảo sát</h4>
                                </div>

                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Chọn kịch bản nội dung</label>
                                        <select class="form-control selectric">
                                            <option>Kịch bản số 1</option>
                                            <option>Kịch bản số 2</option>
                                            <option>Kịch bản số 3</option>
                                            <option>Kịch bản số 4</option>
                                            <option>Kịch bản số 5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Chọn khảo sát</label>
                                        <select class="form-control selectric">
                                            <option>Khảo sát số 1</option>
                                            <option>Khảo sát số 2</option>
                                            <option>Khảo sát số 3</option>
                                            <option>Khảo sát số 4</option>
                                            <option>Khảo sát số 5</option>
                                        </select>
                                    </div>



                                </div>

                            </div>


                        </section>
                        <!--End Kịch bản -->
                        <h3>Step 4 Title</h3>
                        <!--Cấu hình quay số -->
                        <section>

                            <div class="card">
                                <div class="card-header">
                                    <h4>Cấu hình quay số</h4>
                                </div>

                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Số cuộc gọi đồng thời</label>
                                        <input type="text" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Số lần gọi lại</label>
                                        <input type="text" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Thời gian chờ gọi lại (phút)</label>
                                        <input type="text" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label>Đảo số</label>
                                        <select class="form-control selectric">
                                            <option>Không đảo số</option>
                                            <option>Đảo số ngẫu nhiên</option>
                                            <option>Đảo số theo toàn bộ đầu số đang có</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                        </section>
                        <!--End Cấu hình quay số -->
                        <h3>Step 5 Title</h3>
                        <!--Thiết lập gọi ra -->
                        <section>

                            <div class="card">
                                <div class="card-header">
                                    <h4>Thiết lập gọi ra</h4>
                                </div>

                                <div class="card-body">
                                   
                                    <div class="form-group">
                                        <label>Chỉ định nhóm thực hiện chiến dịch</label>
                                        <select class="form-control selectric">
                                            <option>Nhóm Sales</option>
                                            <option>Nhóm Marketing</option>
                                            <option>Nhóm CSKH</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            
                          
                        </section>
                        <!--End thiết lập gọi ra -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Modal thêm chiến dịch-->


@endsection

@push('js')

@endpush

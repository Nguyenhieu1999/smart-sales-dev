@extends('layouts.frontend.master')

@section('title','Home')

@push('css')

@endpush

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Sửa kịch bản cuộc gọi</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{route('updateCallScript')}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden"  class="col-sm-9 form-control" id="cid" name="cid" value="{{$Info->id}}"/>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Tên kịch bản(*)</label>
                                        <input type="text" class="form-control" required="" name="call_script_name" id="call_script_name" value="{{$Info->call_script_name}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Nội dung kịch bản</label>
                                        <div>
                  <textarea class="summernote" name="script" id="script">{{$Info->script}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <div class="form-group">
                                        <label></label>
                                        <div>
                                            <button class="btn btn-primary">Sửa kịch bản</button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('js')

@endpush
